package com.anhth12.sandblasterLBFGS.akka.actor

import akka.actor._
import com.typesafe.config.ConfigFactory

/**
 * Created by Tong Hoang Anh on 4/10/2015.
 */


class PushActor extends Actor with ActorLogging{

  var coordinators: ActorRef = null

  override def receive: Receive = {
    case a: PushActorRequest if coordinators == null =>
      sender ! RetrieveFailed("Service unavailable, try again later")
    case a: PushActorRequest =>
      log.info("Push Actor")
      coordinators forward a
    case PushRegistration =>
      context watch sender
      coordinators = sender
    case Terminated(a) =>
      coordinators = null
  }
}

object PushActor{
  var pushActor:ActorRef = null
  var system: ActorSystem = null

  def startPush(port: String): (ActorSystem,ActorRef) = {
    if (pushActor == null || system == null) {
      val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
        withFallback(ConfigFactory.parseString("akka.cluster.roles = [push]")).
        withFallback(ConfigFactory.load())

      system = ActorSystem("ClusterSystem", config)
      pushActor = system.actorOf(Props[PushActor], name = "push")
    }
    println("Started PUSH ACTOR in port: " + port)
    (system, pushActor)
  }
}

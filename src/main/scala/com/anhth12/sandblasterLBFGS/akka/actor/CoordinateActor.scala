package com.anhth12.sandblasterLBFGS.akka.actor

import akka.actor._
import akka.cluster.ClusterEvent.{CurrentClusterState, MemberUp}
import akka.cluster.protobuf.msg.MemberStatus
import akka.cluster.{Cluster, Member}
import com.anhth12.param.Param
import com.typesafe.config.ConfigFactory

/**
 * Created by Tong Hoang Anh on 4/2/2015.
 */
class CoordinateActor extends Actor with ActorLogging{

  var currentParam: Param = null

  val cluster = Cluster(context.system)

  override def preStart(): Unit = cluster.subscribe(self, classOf[MemberUp])
  override def postStop(): Unit = cluster.unsubscribe(self)

  override def receive: Receive = {
    case bc: BroadcastParam => {
      currentParam = bc.param
    }
    case GetBroadcast =>
      sender ! currentParam
    case Request(param, isFirst) => {
      if(isFirst) {
        log.info("send param to " + sender)
        sender ! BroadcastParam(currentParam)
      }
      else{
        log.info("added new gradient")
        currentParam.addi(param)
      }
    }
    case state: CurrentClusterState =>
      state.members.filter(_.status == MemberStatus.Up) foreach register
    case MemberUp(m) => {
      register(m)
//      sender ! BroadcastParam(currentParam)
    }
  }

  def register(member: Member): Unit =
    if (member.hasRole("replica")){
      context.actorSelection(RootActorPath(member.address) / "user" / "replica") ! ReplicaRegistration
    }
}

object CoordinateActor{

  def startCoordinator(port: String) = {
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [coordinator]")).
      withFallback(ConfigFactory.load())
    val system = ActorSystem("ClusterSystem", config)
//    (system, system.actorOf(Props[CoordinateActor], name = "coordinator"))
    system.actorOf(Props[CoordinateActor], name = "coordinator")
  }
}

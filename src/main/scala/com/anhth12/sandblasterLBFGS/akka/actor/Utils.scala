package com.anhth12.sandblasterLBFGS.akka.actor

import java.net._


/**
 * Created by Tong Hoang Anh on 4/11/2015.
 */
object Utils {
  def getIP = {
    InetAddress.getLocalHost.getHostAddress
  }
}

package com.anhth12.sandblasterLBFGS.akka.actor

import akka.actor._
import akka.cluster.{Member, Cluster}
import akka.cluster.ClusterEvent.{CurrentClusterState, MemberUp}
import akka.cluster.protobuf.msg.MemberStatus
import com.anhth12.param.Param
import com.typesafe.config.ConfigFactory

/**
 * Created by Tong Hoang Anh on 4/10/2015.
 */
class ServiceActor extends Actor with ActorLogging{
  var currentParam: Param = null

  val cluster = Cluster(context.system)

  override def preStart(): Unit = cluster.subscribe(self, classOf[MemberUp])
  override def postStop(): Unit = cluster.unsubscribe(self)


  override def receive: Receive = {
    case bc: BroadcastParam => {
      log.info("set initial parameter")
      currentParam = bc.param
    }
    case PullActorRequest =>{
      log.info("receive pull request")
      sender ! BroadcastParam(currentParam)
    }
    case PushActorRequest(param) => {
      log.info("get push request from " + sender.toString())
      currentParam.addi(param)
    }
    case state: CurrentClusterState =>
      state.members.filter(_.status == MemberStatus.Up) foreach register
    case MemberUp(m) => {
      register(m)
    }
  }

  def register(member: Member): Unit = {
    if (member.hasRole("pull")) {
      context.actorSelection(RootActorPath(member.address) / "user" / "pull") ! PullRegistration
      log.info("registration for pulling")
    }
    if (member.hasRole("push")) {
      context.actorSelection(RootActorPath(member.address) / "user" / "push") ! PushRegistration
      log.info("registration for pushing")
    }
  }
}

object ServiceActor{

  var serviceActor:ActorRef = null
  var system: ActorSystem = null

  def startService(port: String) = {
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=\"" + Utils.getIP + "\"") ).
      withFallback(ConfigFactory.parseString("akka.cluster.role =[service]")).
      withFallback(ConfigFactory.load())
    system = ActorSystem("ClusterSystem", config)
    serviceActor = system.actorOf(Props[ServiceActor], name = "service")
    (system, serviceActor)
  }
}



package com.anhth12.sandblasterLBFGS.akka.actor

import akka.actor._
import com.typesafe.config.ConfigFactory

/**
 * Created by Tong Hoang Anh on 4/10/2015.
 */

class PullActor extends Actor with ActorLogging{

  var coordinators = IndexedSeq.empty[ActorRef]

  override def receive: Receive = {
    case PullActorRequest if coordinators.isEmpty =>
      sender ! RetrieveFailed("Service unavailable, try again later")
    case PullActorRequest =>
      log.info("Pull request")
      coordinators(0) forward PullActorRequest
    case PullRegistration if !coordinators.contains(sender) =>
      context watch sender
      coordinators = coordinators :+ sender
    case Terminated(a) =>
      coordinators = coordinators.filterNot(_ == a)
  }
}

object PullActor{
  var pullActor:ActorRef = null
  var system: ActorSystem = null

  def startPull(port: String): (ActorSystem,ActorRef) = {
    if (pullActor == null || system == null) {
      val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
        withFallback(ConfigFactory.parseString("akka.cluster.roles = [pull]")).
        withFallback(ConfigFactory.load())

      system = ActorSystem("ClusterSystem", config)
      pullActor = system.actorOf(Props[PullActor], name = "pull")
      (system, pullActor)
    }
    println("Started PULL ACTOR in port: " + port)
    (system, pullActor)
  }
}

package com.anhth12.sandblasterLBFGS.akka.actor

import akka.actor._
import com.typesafe.config.ConfigFactory

/**
 * Created by Tong Hoang Anh on 4/2/2015.
 */
class ReplicaActor extends Actor with ActorLogging{

  var coordinators = IndexedSeq.empty[ActorRef]
  var jobCounter = 0


  override def receive: Receive = {
    case pullRequest: Request if coordinators.isEmpty =>
      sender ! RetrieveFailed("Service unavailable, try again later")
    case pullRequest: Request =>
      log.info("forwarding delta to coordinate")
      coordinators(0) forward pullRequest
    case ReplicaRegistration if !coordinators.contains(sender) =>
      context watch sender
      coordinators = coordinators :+ sender
    case Terminated(a) =>
      coordinators = coordinators.filterNot(_ == a)
  }
}

object ReplicaActor{
  var replicaActor:ActorRef = null
  var system: ActorSystem = null

  def startReplica(port: String): (ActorSystem,ActorRef) = {
    if (replicaActor == null || system == null) {
      val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
        withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname = \"" + Utils.getIP + "\"")).
        withFallback(ConfigFactory.parseString("akka.cluster.roles = [replica]")).
        withFallback(ConfigFactory.load())

      system = ActorSystem("ClusterSystem", config)
      replicaActor = system.actorOf(Props[ReplicaActor], name = "replica")
      (system, replicaActor)
    }
    println("Started replicator in port: " + port)
    (system, replicaActor)
  }
}



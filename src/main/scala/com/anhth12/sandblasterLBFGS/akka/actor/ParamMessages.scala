package com.anhth12.sandblasterLBFGS.akka.actor

import com.anhth12.param.Param

/**
 * Created by Tong Hoang Anh on 4/2/2015.
 */
final case class BroadcastParam(param: Param)

final case class Request(param: Param = null, boolean: Boolean = true)

case object PullRegistration

case object PushRegistration

final case class PushActorRequest(val param: Param)

case object PullActorRequest

case object ReplicaRegistration

case object GetBroadcast

final case class RetrieveFailed(reason: String)


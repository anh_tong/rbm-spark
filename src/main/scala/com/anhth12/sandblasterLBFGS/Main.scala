package com.anhth12.sandblasterLBFGS

import _root_.akka.pattern.ask
import _root_.akka.util.Timeout
import com.anhth12.param.Param
import com.anhth12.sandblasterLBFGS.akka.actor._

import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * Created by Tong Hoang Anh on 4/2/2015.
 */
object Main {
  def main(args: Array[String]) {

//    val (serviceSystem,service) = ServiceActor.startService("2551")
//
//    implicit val timeout = Timeout(5 seconds)
//
//    service ? BroadcastParam(new Param(1, 1, 1))
//
////    new Thread(new Runnable {
////      override def run(): Unit = {
////        val (pushSystem, push) = PushActor.startPush("2553")
////        val pushedParam = new Param(1,1,1)
////        pushedParam.hbias().update(0, 1.0)
////        import pushSystem.dispatcher
////        pushSystem.scheduler.schedule(2.seconds, 2.seconds) {
////          push ? PushActorRequest(pushedParam) onSuccess{
////            case RetrieveFailed(s) => println(s)
////          }
////        }
////      }
////    }).start()
//
//    val (pullSystem, pull) = PullActor.startPull("2552")
//
//    import pullSystem.dispatcher
//    pullSystem.scheduler.schedule(2.seconds, 2.seconds) {
//      pull ? PullActorRequest onSuccess {
//        case BroadcastParam(param) => println(param.hbias()(0))
//      }
//    }
//
//    val (pushSystem, push) = PushActor.startPush("2553")
//    val pushedParam = new Param(1,1,1)
//    pushedParam.hbias().update(0, 1.0)
//
//    Thread.sleep(10000)
//    push ? PushActorRequest(pushedParam) onSuccess{
//      case RetrieveFailed(s) => println(s)
//    }






    val coordinator = CoordinateActor.startCoordinator("2551")
   implicit val timeout = Timeout(5 seconds)
    coordinator ? BroadcastParam(new Param(1, 1, 1))
    val (system, replica) = ReplicaActor.startReplica("2552")
    import  system.dispatcher
    Thread.sleep(10000)
    replica ? Request(null, true) onSuccess{
      case RetrieveFailed(s) => println(s)
      case BroadcastParam(param) => println(param.getnHidden().toString)
    }
//    system.scheduler.schedule(2.seconds, 2.seconds) {
//////      implicit val timeout1 = Timeout(5 seconds)
//      replica ? PullRequest onSuccess{
//        case BroadcastParam(param) => println(param.getnHidden().toString)
//      }
//    }
    val pushedParam = new Param(1, 1, 1)
    pushedParam.hbias().update(0, 1.0)
    replica ? Request(pushedParam, false)
    replica ? Request(pushedParam, false)
    replica ? Request(pushedParam, false)

    replica ? Request(null, true) onSuccess{
      case BroadcastParam(param) => println(param.hbias()(0))
    }


  }
}

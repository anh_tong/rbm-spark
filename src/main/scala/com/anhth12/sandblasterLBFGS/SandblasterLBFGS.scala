package com.anhth12.sandblasterLBFGS

import com.anhth12.param.Param
import com.anhth12.rbm.JavaRBM
import com.anhth12.sandblasterLBFGS.akka.actor._
import com.anhth12.rbm.Parser._
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkConf}
import scala.concurrent.duration._
import _root_.akka.util.Timeout
import _root_.akka.pattern.ask


/**
 * Created by Tong Hoang Anh on 4/4/2015.
 */
class SandblasterLBFGS {
  def main (args: Array[String]) {
    val Array(directory, numIterationsStr, miniBatchFractionStr) = args

    val jarFile = getClass.getProtectionDomain.getCodeSource.getLocation.getPath

    val conf = new SparkConf().setAppName("RBM-Netflix-Probe").setJars(Array(jarFile))
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.kryoserializer.buffer.mb", "100")

    val numPartitions = 7
    //    val numIteration = 201
    //    val miniBatchFraction = 0.2



    val numIteration = numIterationsStr.toInt
    val miniBatchFraction = miniBatchFractionStr.toDouble

    val nVisible = 17770
    val sofmax = 5
    val nHidden = 20

    val sc = new SparkContext(conf)

    var javaParam = new com.anhth12.param.Param(nVisible, nHidden, sofmax)

    val probe = loadProbe("/root/word2vec/probe/probe.txt")
    //    val probeBC = sc.broadcast(probe)

    //    doFirstWhenYouDoNotHaveData()

    val probeRDD = sc.parallelize(probe, 7)

    val (train, test) = trainAndTest(directory, probeRDD, sc)
    val groupedTrain = train.groupByKey(64)
    saveRefinedData(groupedTrain, "/user/root/netflix/train.probe")
    val groupedTest = test.groupByKey(7)
    groupedTest.join(groupedTrain).map(a => a._2._1.mkString(",") + "#" + a._2._2.mkString(","))
      .saveAsTextFile("/user/root/netflix/test-join-train.probe")

    val trainData = loadData(sc, "/user/root/netflix/train.probe")

    val testJoinTrain = loadTestJoinTrain(sc, "/user/root/netflix/test-join-train.probe").setName("test-join-train")
    testJoinTrain.cache()
    testJoinTrain.count()

    val movierate1 = trainData.map(_._2).flatMap(a => a.map(t=>t)).map(t => (t, 1)).reduceByKey(_+_)

    val collectMovieRate = movierate1.collectAsMap().map(a => (a._1 :Integer, a._2:Integer))
    javaParam.initWeight()
    import collection.JavaConversions._
    javaParam.initVbias(collectMovieRate)

    val coordinator = CoordinateActor.startCoordinator("10000")
    implicit val timeout = Timeout(5 seconds)
    coordinator ? BroadcastParam(javaParam)

    val repartitioned = trainData.map(_._2).repartition(numPartitions).persist(StorageLevel.MEMORY_AND_DISK).setName("data-after-partition")
    var start = System.currentTimeMillis();

      val result = repartitioned
        .foreachPartition(t => {
        JavaRBM.nVisbile = nVisible
        JavaRBM.nHidden = nHidden
        JavaRBM.softmax = sofmax
        val javaRBM = JavaRBM.getInstance()
        val (system, replica) = ReplicaActor.startReplica("10001")
        for (iteration <- 0 until numIteration) {
//          val broadcastParam = replica ? Request(null, true)
          import system.dispatcher
          replica ? Request(null, true) onSuccess {
            case BroadcastParam(param) => javaRBM.setParam(param)
          }
          val deltaParam = javaRBM.train(t.toList, iteration)
          replica ? Request(deltaParam, false)
        }
      }
        )

//      if (iteration % 5 == 0) {
//        coordinator ? GetBroadcast onSuccess {
//          case param: Param => {
//            val (nrmse, numTest) = testJoinTrain.map(a => JavaRBM.evaluate(a._1, a._2, param, nVisible, nHidden, sofmax))
//              .reduce((a, b) => (a._1 + b._1, a._2 + b._2))
//            var end = System.currentTimeMillis()
//            println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
//            printToFile(new java.io.File(iteration.toString + ".txt")) {
//              p => p.println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
//            }
//            start = System.currentTimeMillis()
//          }
//        }
//      }


    def zeroValue() = new Param(nVisible, nHidden, sofmax)

  }
  def saveToTextFile(dat: RDD[Array[Int]], hadoopFile: String) = {
    dat.map(a => a.mkString(",")).saveAsTextFile(hadoopFile)
  }
  def load(context: SparkContext, hadoopFile: String) = {
    context.wholeTextFiles(hadoopFile, 8).map(_._2)
      .flatMap(s => s.split("\n")).map(s => s.split(",").map(a => a.toInt))
  }

  def getMovieRating(input: Int): (Int, Int)={
    (input/10, input %10)
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
  }
}

package com.anhth12.rbm

import com.anhth12.param.Param
import com.anhth12.rbm.{DataInfo, JavaRBM}
import com.anhth12.rbm.MovieLensParser._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.SparkContext._

import scala.util.Random

/**
 * Created by Tong Hoang Anh on 3/30/2015.
 */
object MovieLens {
  def main(args: Array[String]) {
    val Array(directory) = args

    val jarFile = getClass.getProtectionDomain.getCodeSource.getLocation.getPath

    val conf = new SparkConf().setAppName("RBM-MovieLens").setJars(Array(jarFile))
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.kryoserializer.buffer.mb", "100")

    val numPartitions = 7
    val numIteration = 201
    val miniBatchFraction = 0.3

    val nVisible = 10681
    val sofmax = 10
    val nHidden = 90

    val sc = new SparkContext(conf)

    var javaParam = new Param(nVisible, nHidden, sofmax)


    //    doFirstWhenYouDoNotHaveData()

    val trainData = loadData(sc, "/user/root/movielens/ra.train.RBM").groupByKey(64).map(a => (a._1, a._2.toArray))
        val testData = loadData(sc, "/user/root/movielens/ra.test.RBM").groupByKey(64).map(a => (a._1, a._2.toArray)).join(trainData)
          .map(a => a._2._1.mkString(",") + "#" + a._2._2.mkString(","))
          .saveAsTextFile("/user/root/movielens/test-join-train.dat")
    val testJoinTrain = Parser.loadTestJoinTrain(sc, "/user/root/movielens/test-join-train.dat")
    testJoinTrain.cache()
    testJoinTrain.count()

    val movierate1 = trainData.map(_._2).flatMap(a => a.map(t => t)).map(t => (t, 1)).reduceByKey(_ + _)

    val collectMovieRate = movierate1.collectAsMap().map(a => (a._1: Integer, a._2: Integer))
    javaParam.initWeight()
    import collection.JavaConversions._
    javaParam.initVbias(collectMovieRate)

    val repartitioned = trainData.map(_._2).repartition(numPartitions).persist(StorageLevel.MEMORY_AND_DISK).setName("data-after-partition")
    var start = System.currentTimeMillis();
    for (iteration <- 0 until numIteration) {
      val javaParamBC = sc.broadcast(javaParam)
      val result = repartitioned
        .sample(false, miniBatchFraction, 42 + iteration)
        .mapPartitions(
          t =>
            Iterator(t)
        )
        .map(t => {
        DataInfo.epsilonW = 0.001
        DataInfo.epsilonHB = 0.008
        DataInfo.epsilonVB = 0.006
        DataInfo.weightCost
        JavaRBM.nVisbile = nVisible
        JavaRBM.nHidden = nHidden
        JavaRBM.softmax = sofmax
        val javaRBM = JavaRBM.getInstance()

        javaRBM.setParam(javaParamBC.value)
        javaRBM.train(t.toList, iteration)
      })
      val collectedResult = result.collect()

      collectedResult.foreach(newParam => {
        javaParam.addi(newParam)
      })

      //        .aggregate(zeroValue())(
      //        seqOp = (c, v) => {
      //          val javaRBM = new JavaRBM(nVisible, sofmax, nHidden)
      //          javaRBM.setParam(javaParamBC.value)
      //          val gradient = javaRBM.train(v.toList, iteration)
      //          c.addi(gradient)
      //        },
      //        combOp = (c1, c2) => {
      //          c1.addi(c2)
      //        }
      //      )
      //      javaParam.addi(result.scale(1.0 /numPartitions))
      //        javaParam.addi(result)

      if (iteration % 5 == 0) {
        val (nrmse, numTest) = testJoinTrain.map(a => JavaRBM.evaluate(a._1, a._2, javaParam, nVisible, nHidden, sofmax))
          .aggregate((0.0D, 0))(
            seqOp = (c, v) => {
              (c._1 + v._1, c._2 + v._2)
            },
            combOp = (c1, c2) => {
              (c1._1 + c2._1, c1._2 + c2._2)
            }
          )
        var end = System.currentTimeMillis()
        println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
        printToFile(new java.io.File("MovieLens-" + iteration.toString + ".txt")) {
          p => p.println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
        }
      }
      start = System.currentTimeMillis()

    }

    def zeroValue() = new Param(nVisible, nHidden, sofmax)

  }

  def saveToTextFile(dat: RDD[Array[Int]], hadoopFile: String) = {
    dat.map(a => a.mkString(",")).saveAsTextFile(hadoopFile)
  }

  def load(context: SparkContext, hadoopFile: String) = {
    context.wholeTextFiles(hadoopFile, 8).map(_._2)
      .flatMap(s => s.split("\n")).map(s => s.split(",").map(a => a.toInt))
  }

  def getMovieRating(input: Int): (Int, Int) = {
    (input / 10, input % 10)
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try {
      op(p)
    } finally {
      p.close()
    }
  }
}


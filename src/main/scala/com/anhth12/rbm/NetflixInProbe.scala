package com.anhth12.rbm

import _root_.akka.pattern.ask
import _root_.akka.util.Timeout
import com.anhth12.param.Param
import com.anhth12.rbm.{ConditionalCFRBM, CFRBMwithIndex, CFRBM, JavaRBM}
import com.anhth12.sandblasterLBFGS.akka.actor._
import com.anhth12.rbm.Parser._
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{Logging, SparkConf, SparkContext}

import scala.concurrent.duration._


/**
 * Created by Tong Hoang Anh on 3/31/2015.
 */
object NetflixInProbe extends Logging{

  def main(args: Array[String]) {
    val Array(directory, numIterationsStr, miniBatchFractionStr, numPartitionsStr) = args

    val conf = new SparkConf()
      .setAppName("RBM-Netflix-Probe")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.kryoserializer.buffer.mb", "100")


    val numPartitions = numPartitionsStr.toInt
    val numIteration = numIterationsStr.toInt
    val miniBatchFraction = miniBatchFractionStr.toDouble

    val nVisible = 17770
    val sofmax = 5
    val nHidden = 100

    val sc = new SparkContext(conf)

    var javaParam = new com.anhth12.param.Param(nVisible, nHidden, sofmax)

//        val probe = loadProbe("/root/word2vec/probe/probe.txt")
//        val probeBC = sc.broadcast(probe)

    //    doFirstWhenYouDoNotHaveData()

//        val probeRDD = sc.parallelize(probe, 7)

//        val (train, test) = trainAndTest(directory, probeRDD, sc)
//        val groupedTrain = train.groupByKey(64)
    //    saveRefinedData(groupedTrain, "/user/root/netflix/train.probe")
    //    val groupedTest = test.groupByKey(7)
    //    groupedTest.join(groupedTrain).map(a => a._2._1.mkString(",") + "#" + a._2._2.mkString(","))
    //      .saveAsTextFile("/user/root/netflix/test-join-train.probe")

    val trainData = loadData(sc, "/user/root/netflix/train.probe")
    //        val trainData = sc.parallelize(scala.io.Source.fromFile("D:\\train\\train.txt").getLines().toSeq).map(s => {
    //  val splitted = s.split(",")
    //  val user = splitted(0).toInt
    //  val rates = splitted.slice(1, splitted.length).map(s => s.toInt)
    //  (user, rates)
    //})


    val testJoinTrain = loadTestJoinTrain(sc, "/user/root/netflix/test-join-train.probe").setName("test-join-train")
      //val testJoinTrain = sc.parallelize(scala.io.Source.fromFile("D:\\test\\test.txt").getLines().toSeq)
      //  .map(s => {
      //  val trainTestSplitted = s.split("#")
      //  val train = trainTestSplitted(0).split(",").map(s => s.toInt)
      //  val test = trainTestSplitted(1).split(",").map(s => s.toInt)
      //  (train, test)
      //})
      .setName("test-join-train")
    testJoinTrain.cache()
    testJoinTrain.count()

    val movierate1 = trainData.map(_._2).flatMap(a => a.map(t => t)).map(t => (t, 1)).reduceByKey(_ + _)

    val collectMovieRate = movierate1.collectAsMap().map(a => (a._1: Integer, a._2: Integer))
    javaParam.initWeight()
    import collection.JavaConversions._
    javaParam.initVbias(collectMovieRate)


    synchronousWithConditional()

    def asynchronous() = {
      val repartitioned = trainData.map(_._2).repartition(numPartitions).persist(StorageLevel.MEMORY_AND_DISK).setName("data-after-partition")

      val coordinator = CoordinateActor.startCoordinator("55555")
      implicit val timeout = Timeout(5 seconds)
      coordinator ? BroadcastParam(javaParam)

      repartitioned.foreachPartition(
        t => {
          ReplicaActor.startReplica("0")
          log.info("starting replicator")
        }
      )
      //wait for all Replicator starting up
      Thread.sleep(10000)

      repartitioned
        .mapPartitions(t => Iterator(t))
        .map(
          t => {
            var success = true;
            var broadcasted: Param = null
            val system = ReplicaActor.system
            val replica = ReplicaActor.replicaActor

            import system.dispatcher
            for (i <- 0 until 10) {
              log.info("current iteration: " + i)
              broadcasted = null
              replica ? Request(null, true) onSuccess {
                case RetrieveFailed(s) =>
                  log.info(s)
                  success = false
                case BroadcastParam(param) =>
                  log.info("got broadcasted param")
                  broadcasted = param
              }
              if (broadcasted != null) {
                CFRBM.M = nVisible
                CFRBM.H = nHidden
                CFRBM.R = sofmax
                val rbm = CFRBM.getInstance();
                rbm.setParam(broadcasted)
                val gradient = rbm.train(t.toList, i)
                replica ? Request(gradient, false)
              }
            }
            success
          }
        ).collect()
    }

    def synchronous() = {
      val repartitioned = trainData.map(_._2).repartition(numPartitions).persist(StorageLevel.MEMORY_AND_DISK).setName("data-after-partition")

      var start = System.currentTimeMillis();
      for (iteration <- 0 until numIteration) {
        val javaParamBC = sc.broadcast(javaParam)
        val result = repartitioned
          .sample(false, miniBatchFraction, 42 + iteration)
          .mapPartitions(
            t =>
              Iterator(t)
          )
          .map(t => {
          CFRBM.M = nVisible
          CFRBM.H = nHidden
          CFRBM.R = sofmax

          val rbm = CFRBM.getInstance();
          rbm.setParam(javaParamBC.value)
          rbm.train(t.toList, iteration)
        })
        val collectedResult = result.collect()

        collectedResult.foreach(newParam => {
          javaParam.addi(newParam)
        })
        if (iteration % 5 == 0 && iteration != 0) {
          val (nrmse, numTest) = testJoinTrain.map(a => JavaRBM.evaluate(a._2, a._1, javaParam, nVisible, nHidden, sofmax))
            .reduce((a, b) => {
            (a._1 + b._1, a._2 + b._2)
          })
          var end = System.currentTimeMillis()
          println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
          printToFile(new java.io.File(iteration.toString + ".txt")) {
            p => p.println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
          }
          start = System.currentTimeMillis()
        }


      }
    }

    def synchronousWithIndex() = {
      val repartitioned = trainData.map(_._2).repartition(numPartitions).persist(StorageLevel.MEMORY_AND_DISK).setName("data-after-partition")

      var start = System.currentTimeMillis();
      for (iteration <- 0 until numIteration) {
        val javaParamBC = sc.broadcast(javaParam)
        val result = repartitioned
          .sample(false, miniBatchFraction, 42 + iteration)
          .mapPartitions(
            t =>
              Iterator(t)
          ).zipWithIndex()
          .map { case (trainData, index) => {
          if (iteration == 0) {
            val rbm = new CFRBMwithIndex(nVisible, nHidden, sofmax)
            RBMManager.setRBM(index, rbm)
          }
          val rbm = RBMManager.getRBM(index)
          rbm.setParam(javaParamBC.value)
          rbm.train(trainData.toList, iteration)
        }
        }
        val collectedResult = result.collect()

        collectedResult.foreach(newParam => {
          javaParam.addi(newParam)
        })
        if (iteration % 5 == 0 && iteration != 0) {
          val (nrmse, numTest) = testJoinTrain.map(a => JavaRBM.evaluate(a._2, a._1, javaParam, nVisible, nHidden, sofmax))
            .reduce((a, b) => {
            (a._1 + b._1, a._2 + b._2)
          })
          var end = System.currentTimeMillis()
          println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
          printToFile(new java.io.File(iteration.toString + ".txt")) {
            p => p.println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
          }
          start = System.currentTimeMillis()
        }
      }
    }

    def synchronousWithConditional() = {

      val rateToSave = parseRaw("/user/root/netflix/train.probe", sc).groupByKey(7).map{
        case(user, data) =>
          (user, data.map(a => a / 10:Int).toArray)
      }

      rateToSave.map{
        case(user, data) =>
          user + " " + data.mkString(",")
      }.saveAsTextFile("user/root/netflix/rate.probe")

      val rate = sc.textFile("/user/root/netflix/rate.probe").map{
        line =>
          val splitted = line.split(" ")
          val dataArray = splitted(1).split(",")map(a => a.toInt)
          (splitted(0).toInt, dataArray.toArray)
      }

      val trainJoinRate = trainData.join(rate)
//      val trainAndRate = generateRateData(trainData)

      val repartitioned = trainJoinRate
        .map(_._2)
        .repartition(numPartitions)
        .persist(StorageLevel.MEMORY_AND_DISK)
        .setName("data-after-partition")

      var start = System.currentTimeMillis();
      for (iteration <- 0 until numIteration) {
        val javaParamBC = sc.broadcast(javaParam)
        val result = repartitioned
          .sample(false, miniBatchFraction, 42 + iteration)
          .mapPartitions(
            t =>
              Iterator(t)
          ).zipWithIndex()
          .map { case (trainData, index) => {
          if (iteration == 0) {
            val rbm = new ConditionalCFRBM(nVisible, nHidden, sofmax)
            ConditionalRBMManager.setRBM(index, rbm)
          }
          val rbm = ConditionalRBMManager.getRBM(index)
          rbm.setParam(javaParamBC.value)
          rbm.train(trainData.toList, iteration)
        }
        }
        val collectedResult = result.collect()

        collectedResult.foreach(newParam => {
          javaParam.addi(newParam)
        })
        if (iteration % 5 == 0 && iteration != 0) {
          val (nrmse, numTest) = testJoinTrain.map(a => JavaRBM.evaluate(a._2, a._1, javaParam, nVisible, nHidden, sofmax))
            .reduce((a, b) => {
            (a._1 + b._1, a._2 + b._2)
          })
          var end = System.currentTimeMillis()
          println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
          printToFile(new java.io.File(iteration.toString + ".txt")) {
            p => p.println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
          }
          start = System.currentTimeMillis()
        }
      }
    }
  }

  def generateRateData(trainData: RDD[(Int, Array[Int])]): RDD[(Int, Array[Int], Array[Int])] ={
    trainData.map{
      case(user, data) =>
        (user, data, data.map(movierate => (movierate / 10): Int))
    }
  }

  def saveToTextFile(dat: RDD[Array[Int]], hadoopFile: String) = {
    dat.map(a => a.mkString(",")).saveAsTextFile(hadoopFile)
  }

  def load(context: SparkContext, hadoopFile: String) = {
    context.wholeTextFiles(hadoopFile, 8).map(_._2)
      .flatMap(s => s.split("\n")).map(s => s.split(",").map(a => a.toInt))
  }

  def getMovieRating(input: Int): (Int, Int) = {
    (input / 10, input % 10)
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try {
      op(p)
    } finally {
      p.close()
    }
  }

}

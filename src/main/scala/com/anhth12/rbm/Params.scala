package com.anhth12.rbm

/**
 * Created by Tong Hoang Anh on 3/20/2015.
 */
class Params (val weight: Array[Array[Array[Double]]],
              val vbias: Array[Array[Double]],
             val hbias: Array[Double]
               ) extends Serializable{
  def addi(newParam: Params, nVisible: Int, nHidden: Int, softmax: Int): Params ={
    for(visible <- 0 until nVisible){
      for(hidden <-0 until nHidden){
        for(rate <- 0 until softmax){
          weight(visible)(rate)(hidden) += newParam.weight(visible)(rate)(hidden)
        }
      }

    }
    for(hidden <- 0 until nHidden){
      hbias(hidden) += newParam.hbias(hidden)
    }

    for(visible <-0 until nVisible) {
      for (rate <- 0 until softmax) {
        vbias(visible)(rate) += newParam.vbias(visible)(rate)
      }
    }
    this
  }

  def scale(s: Double, nVisible: Int, nHidden: Int, softmax: Int) = {
    for(visible <- 0 until nVisible) {
      for (hidden <- 0 until nHidden) {
        for (rate <- 0 until softmax) {
          weight(visible)(rate)(hidden) *= s
        }
      }
    }

    for(hidden <- 0 until nHidden){
      hbias(hidden) *= s
    }

    for(visible <-0 until nVisible) {
      for (rate <- 0 until softmax) {
        vbias(visible)(rate) *= s
      }
    }
  }

  def duplicate() ={
    new Params(weight.clone(), vbias.clone(), hbias.clone())
  }
}

object Params extends Serializable{
  def zeros(nVisible: Int, nHidden: Int, softmax: Int) = {
    new Params(Array.ofDim[Double](nVisible, softmax, nHidden), Array.ofDim[Double](nVisible,softmax), Array.ofDim[Double](nHidden))
  }

  def initParams(nVisible: Int, nHidden: Int, softmax: Int) = new Params(Util.initWeight(nVisible, nHidden, softmax),
    Util.initVBias(nVisible, softmax),
    Util.initHBias(nHidden))
}

package com.anhth12.rbm

import com.anhth12.rbm.{ConditionalCFRBM, CFRBMwithIndex}
import org.apache.spark.Logging

import scala.collection.mutable.HashMap

/**
 * Created by Tong Hoang Anh on 4/13/2015.
 */
object RBMManager extends Logging{
  var rbms = new HashMap[Long, CFRBMwithIndex]

  def setRBM(index: Long, model: CFRBMwithIndex): Unit ={
    if(!rbms.contains(index)){
      log.info(s"put model with index $index for the first time")
      rbms.put(index, model)
    }else{
      log.error(s"already put model with index $index")
    }

  }

  def getRBM(index: Long): CFRBMwithIndex = {
    if(!rbms.contains(index)){
      throw new Exception(s"there is no RBM for index $index")
    }
    rbms.get(index).get
  }

}

object ConditionalRBMManager extends Logging{
  var rbms = new HashMap[Long, ConditionalCFRBM]

  def setRBM(index: Long, model: ConditionalCFRBM): Unit ={
    if(!rbms.contains(index)){
      log.info(s"put model with index $index for the first time")
      rbms.put(index, model)
    }else{
      log.error(s"already put model with index $index")
    }

  }

  def getRBM(index: Long): ConditionalCFRBM = {
    if(!rbms.contains(index)){
      throw new Exception(s"there is no RBM for index $index")
    }
    rbms.get(index).get
  }

}

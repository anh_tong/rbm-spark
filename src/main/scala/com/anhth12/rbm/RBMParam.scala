package com.anhth12.rbm

/**
 * Created by Tong Hoang Anh on 3/25/2015.
 */
class RBMParam {
  var epsilonVb = 0.08
  var epsilonW = 0.001
  var epsilonHb = 0.06

  var weightCost = 0.01
  var momentum   = 0.8;
}

package com.anhth12.rbm

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD

import scala.collection.mutable.ArrayBuffer

/**
 * Created by Tong Hoang Anh on 3/26/2015.
 */
object Parser {
  /**
   * Parse raw data from Netflix dataset
   *
   * @param hadoopFile hadoop FS directory
   * @param sc current spark context
   * return (user, (movie - 1)*10 + rating - 1)
   */
  def parseRaw(hadoopFile: String, sc: SparkContext): RDD[(Int, Int)] = {
    sc.wholeTextFiles(hadoopFile, 64).map(_._2).flatMap {
      file =>
        val splitted = file.split("\n")
        var flag = true
        var first = ""
        val map = splitted.map {
          line =>
            if (flag) {
              first = line
              flag = false
            }
            (first.substring(0, first.length - 1).toInt, line)
        }
        map
    }.filter(a => !a._2.endsWith(":"))
      .map(pair => {
      val splitted = pair._2.split(",")
      //user, movie, rating
      (splitted(0).toInt, (pair._1, splitted(1).toInt))
    }).map(a => (a._1, ((a._2._1 - 1) * 10 + (a._2._2 - 1))))
//      .groupByKey(64).map(a => a._2.toArray)
  }

  def loadProbe(filePath: String) = {
    var currentMovie = -1
    val arrayBufferTuple = new ArrayBuffer[(Int, Int)]()
    scala.io.Source.fromFile(filePath).getLines().foreach(line => {
      if(line.endsWith(":")){
        currentMovie = line.substring(0, line.length - 1).toInt
      }else{
        arrayBufferTuple.append((line.toInt, currentMovie))
      }
    })
    arrayBufferTuple.toList
  }

//  def trainAndTest(hadoopFile: String, probe: Broadcast[List[(Int, Int)]], sc: SparkContext) = {
//    val input = sc.wholeTextFiles(hadoopFile, 64).map(_._2).flatMap {
//      file =>
//        val splitted = file.split("\n")
//        var flag = true
//        var first = ""
//        val map = splitted.map {
//          line =>
//            if (flag) {
//              first = line
//              flag = false
//            }
//            (first.substring(0, first.length - 1).toInt, line)
//        }
//        map
//    }.filter(a => !a._2.endsWith(":"))
//      .map(pair => {
//      val splitted = pair._2.split(",")
//      //user, movie, rating
//      ((splitted(0).toInt, pair._1), splitted(1).toInt)
//    })
//
//
//    val train = input.filter(a => !probe.value.contains(a._1)).map(a => (a._1._1, (a._1._2 - 1) * 10 + a._2 - 1))
//    val test = input.filter(a=> probe.value.contains(a._1)).map(a => (a._1._1, (a._1._2 - 1) * 10 + a._2 - 1))
//    (train, test)
//  }

  def trainAndTest(hadoopFile: String, probe: RDD[(Int, Int)], sc: SparkContext) = {
    val input = sc.wholeTextFiles(hadoopFile, 64).map(_._2).flatMap {
      file =>
        val splitted = file.split("\n")
        var flag = true
        var first = ""
        val map = splitted.map {
          line =>
            if (flag) {
              first = line
              flag = false
            }
            (first.substring(0, first.length - 1).toInt, line)
        }
        map
    }.filter(a => !a._2.endsWith(":"))
      .map(pair => {
      val splitted = pair._2.split(",")
      //user, movie, rating
      ((splitted(0).toInt, pair._1), splitted(1).toInt)
    })
    val probeRate = probe.map(a =>(a, 0))
    val test = probeRate.join(input).map(a => (a._1, a._2._1 + a._2._2))
    val train = input.subtract(test)
    (train.map(a => (a._1._1, (a._1._2 - 1) * 10 + a._2 - 1)), test.map(a => (a._1._1, (a._1._2 - 1) * 10 + a._2 - 1)))
  }

  /**
   * Group ratings by user
   * @param input user rating RDD
   * @param numPartition the number of partitions
   * @return grouped dataset
   */
  def groupByUser(input: RDD[(Int, Int)], numPartition: Int):RDD[(Int, Iterable[Int])] = {
    return input.groupByKey(numPartition)
  }

  /**
   * Save refined data into HDFS
   *
   * @param data refined RDD
   * @param hadoopFile destination HDFS file
   */
  def saveRefinedData(data: RDD[(Int, Iterable[Int])], hadoopFile: String): Unit = {
    data.map(userdata => userdata._1 + "," + userdata._2.mkString(",")).saveAsTextFile(hadoopFile)
  }

  /**
   * Load train data
   * @param sc Spark Context
   * @param hadoopFile HDFS directory
   * @return RDD tuple2 Int and Array of Int
   */
  def loadData(sc: SparkContext, hadoopFile: String): RDD[(Int,Array[Int])] = {
    sc.wholeTextFiles(hadoopFile, 64).map(_._2).flatMap(s => s.split("\n")).map(s => {
      val splitted = s.split(",")
      val user = splitted(0).toInt
      val rates = splitted.slice(1, splitted.length).map(s => s.toInt)
      (user, rates)
    })
  }

  def loadTestJoinTrain(sc: SparkContext, hadoopFile: String): RDD[(Array[Int], Array[Int])] = {
    sc.wholeTextFiles(hadoopFile, 64).map(_._2).flatMap(s => s.split("\n")).map(s => {
      val trainTestSplitted = s.split("#")
      val train = trainTestSplitted(0).split(",").map(s => s.toInt)
      val test = trainTestSplitted(1).split(",").map(s => s.toInt)
      (train, test)
    })
  }

  def main(args: Array[String]) {
    loadProbe("C:\\Users\\anhth5\\Downloads\\download\\probe.txt")
  }






}

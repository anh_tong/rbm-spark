package com.anhth12.rbm

import com.anhth12.param.{TwoDimensionArray, ThreeDimensionArray, OneDimensionArray, Param}
import org.apache.spark.Logging
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel

import scala.collection.mutable.ArrayBuffer
import scala.util.Random
import scala.util.control.Breaks._

/**
 * Created by Tong Hoang Anh on 3/20/2015.
 */
class RBM (val nVisible: Int, val nHidden: Int, val softmax: Int) extends Serializable with Logging{
//  val positiveHiddenActivation = new OneDimensionArray(nHidden)
//  val positiveHiddenStates = Array.ofDim[Char](nHidden)
//  val negativeHiddenStates = Array.ofDim[Char](nHidden)
//  val negativeHiddenActivate = new OneDimensionArray(nHidden)
//  val CDpositive = new ThreeDimensionArray(nVisible, softmax, nHidden)
////  val CDpositive = Array.ofDim[Double](nVisible, softmax, nHidden)
//  val CDnegative = new ThreeDimensionArray(nVisible, softmax,nHidden)
//  val CDinc = new ThreeDimensionArray(nVisible, softmax, nHidden)
////  val CDnegative = Array.ofDim[Double](nVisible, softmax, nHidden)
//
////  val positiveVisibleActivate = Array.ofDim[Double](nVisible, softmax)
//  val positiveVisibleActivate = new TwoDimensionArray(nVisible, softmax)
//  val negativeVisibleSoftmax = Array.ofDim[Char](nVisible)
////  val negativeVisibleActivation = Array.ofDim[Double](nVisible, softmax)\
//  val negativeVisibleActivation = new TwoDimensionArray(nVisible, softmax)
////  val negativeVisibleProbs = Array.ofDim[Double](nVisible, softmax)
//  val negativeVisibleProbs = new TwoDimensionArray(nVisible, softmax)


  def learn(input: List[Array[Int]], cdK: Int, param: Param, rbmParam: RBMParam, iteration: Int) ={

    var tSteps = 1

    if (iteration >= 10) {
      tSteps = 3 + (iteration - 10) / 5;
    }

    RBM.sumW.zeroItself()
    RBM.negativeVisibleProbs.zeroItself()


    val errors = new ArrayBuffer[Double](input.length);

    val batchSize = input.length
    logInfo("receive batchSize: " + batchSize)

    var tc : Long = 0L
    var nrmse: Double = 0.0D

    zeros()

    for(user <- 0 to batchSize - 1){
      val num = input(user).length

      RBM.sumW.zeroItself()
      RBM.negativeVisibleProbs.zeroItself()
      /**START activate hidden**/
      for(i <- 0 until num){
        val (movie, rating) = movierating(input(user)(i))
        RBM.positiveVisibleActivate.add(1.0, movie, rating);
        for(hidden <- 0 until nHidden){
          RBM.sumW.add(param.getWeight(movie, rating, hidden), hidden)
//            RBM.sumW(hidden) += param.getWeight(movie, rating, hidden)
        }
      }
      /**END activate hidden**/
      /**START sample hidden**/

      for(hidden <- 0 until nHidden){
        val probs : Double = sigmoid(RBM.sumW.get(hidden) + param.getHbias(hidden))
        if(probs > Random.nextDouble()){
          RBM.positiveHiddenActivation.add(1.0, hidden)
          RBM.positiveHiddenStates(hidden) = 1
        }else{
          RBM.positiveHiddenStates(hidden) = 0
        }
      }
      /**END sample hidden**/

      for(hidden <- 0 until nHidden){
        RBM.currentPositiveHiddenStatas(hidden) = RBM.positiveHiddenStates(hidden)
      }


      /**START contrastive divergence **/

      var stepT = 0
      do{
        val finalTStep = (stepT + 1 >= tSteps)
        for(i<-0 until num){
          val movie : Int = input(user)(i) /10
          for(hidden <- 0 until nHidden){
            if(RBM.currentPositiveHiddenStatas(hidden) == 1){
              for(rate <- 0 until softmax){
                RBM.negativeVisibleProbs.add(param.getWeight(movie, rate, hidden), movie,rate)
              }
            }
          }
          var tsum :Double = 0.0

          for(rate <- 0 until softmax){
            RBM.negativeVisibleProbs.set(sigmoid(RBM.negativeVisibleProbs.get(movie,rate) + param.getVbias(movie,rate)), movie,rate)
            tsum += RBM.negativeVisibleProbs.get(movie,rate)
         }

          // normalize
          if (tsum != 0){
            for(rate <- 0 until softmax){
              RBM.negativeVisibleProbs.set(RBM.negativeVisibleProbs.get(movie, rate) / tsum, movie, rate)
              //              negativeVisibleProbs(movie)(rate) /= tsum
            }
          }

          // sampling softmax
          var randVal = Random.nextDouble
          breakable{
            for(rate<- 0 until softmax){
              randVal -= RBM.negativeVisibleProbs.get(movie, rate)
              if(randVal <= 0.0){
                RBM.negativeVisibleSoftmax(movie) = rate.toChar
                break()
              }
              RBM.negativeVisibleSoftmax(movie) = (softmax - 1).toChar;
            }
          }

          if(finalTStep){
            RBM.negativeVisibleActivation.add(1.0, movie,RBM.negativeVisibleSoftmax(movie))
          }
        }
        RBM.sumW.zeroItself();
        //        val sumW = Array.ofDim[Double](nHidden)
        for(i<- 0 until num){
          val movie: Int = input(user)(i)/10
          for(hidden <- 0 until nHidden){
            RBM.sumW.add(param.getWeight(movie,RBM.negativeVisibleSoftmax(movie),hidden), hidden)
            //            sumW(hidden) = param.getWeight(movie,RBM.negativeVisibleSoftmax(movie),hidden)
          }
        }

        for(hidden <-0 until nHidden){
          val probs: Double = sigmoid(RBM.sumW.get(hidden) + param.getHbias(hidden))

          if(probs > Random.nextDouble()){
            RBM.negativeHiddenStates(hidden) = 1;
            if(finalTStep){
              //              negativeHiddenActivate(hidden) += 1.0
              RBM.negativeHiddenActivate.add(1.0, hidden)
            }
          }
          else{
            RBM.negativeHiddenStates(hidden) = 0
          }
        }

        if(!finalTStep){
          for(hidden <- 0 until nHidden){
            RBM.currentPositiveHiddenStatas(hidden) = RBM.negativeHiddenStates(hidden)
          }
          RBM.negativeVisibleProbs.zeroItself();
        }
        stepT +=1
      }while(stepT < tSteps)
//      for(k <- 0 until cdK){
//        /**START active visible**/
//
//        for(i<-0 until num){
//          val movie : Int = input(user)(i) /10
//          for(hidden <- 0 until nHidden){
//            if(RBM.currentPositiveHiddenStatas(hidden) == 1){
//              for(rate <- 0 until softmax){
//                RBM.negativeVisibleProbs.add(param.getWeight(movie, rate, hidden), movie,rate)
//              }
//            }
//          }
//
//          for(rate <- 0 until softmax){
//            RBM.negativeVisibleProbs.set(1.0 / (1 + math.exp( - RBM.negativeVisibleProbs.get(movie,rate) - param.getVbias(movie,rate))), movie,rate)
//          }
//          var tsum :Double = 0.0
//          for(rate <- 0 until softmax){
//            tsum += RBM.negativeVisibleProbs.get(movie,rate)
//          }
//
//          if (tsum != 0){
//            for(rate <- 0 until softmax){
//              RBM.negativeVisibleProbs.set(RBM.negativeVisibleProbs.get(movie, rate) / tsum, movie, rate)
////              negativeVisibleProbs(movie)(rate) /= tsum
//            }
//          }
//
//          var randVal = Random.nextDouble
//          breakable{
//            for(rate<- 0 until softmax){
//              randVal -= RBM.negativeVisibleProbs.get(movie, rate)
//              if(randVal <= 0.0){
//                RBM.negativeVisibleSoftmax(movie) = rate.toChar
//                break()
//              }
//              RBM.negativeVisibleSoftmax(movie) = (softmax - 1).toChar;
//            }
//          }
//
//          if(k == cdK - 1){
//            RBM.negativeVisibleActivation.add(1.0, movie,RBM.negativeVisibleSoftmax(movie))
//          }
//        }
//        RBM.sumW.zeroItself();
////        val sumW = Array.ofDim[Double](nHidden)
//        for(i<- 0 until num){
//          val movie: Int = input(user)(i)/10
//          for(hidden <- 0 until nHidden){
//            RBM.sumW.add(param.getWeight(movie,RBM.negativeVisibleSoftmax(movie),hidden), hidden)
////            sumW(hidden) = param.getWeight(movie,RBM.negativeVisibleSoftmax(movie),hidden)
//          }
//        }
//
//        for(hidden <-0 until nHidden){
//          val probs: Double = 1.0 / (1.0 + math.exp( - RBM.sumW.get(hidden) - param.getHbias(hidden)))
//
//          if(probs > Random.nextDouble()){
//            RBM.negativeHiddenStates(hidden) = 1;
//            if(k == cdK -1){
////              negativeHiddenActivate(hidden) += 1.0
//              RBM.negativeHiddenActivate.add(1.0, hidden)
//            }else{
//              RBM.negativeHiddenStates(hidden) = 0
//            }
//          }
//        }
//
//        if(k != cdK - 1){
//          for(hidden <- 0 until nHidden){
//            RBM.currentPositiveHiddenStatas(hidden) = RBM.negativeHiddenStates(hidden)
//          }
//          RBM.negativeVisibleProbs.zeroItself();
//        }
//      }
      //END CD

      tc += num
      for(i <- 0 until num) {
        val (movie, rate) = movierating(input(user)(i))

        for (hidden <- 0 until nHidden) {
          if (RBM.positiveHiddenStates(hidden) == 1) {
            RBM.CDpositive.add(1.0, movie, rate, hidden)
//            CDpositive(movie)(rate)(hidden) += 1.0
          }
          RBM.CDnegative.add(RBM.negativeHiddenStates(hidden), movie, RBM.negativeVisibleSoftmax(movie), hidden)
//          CDnegative(movie)(negativeVisibleSoftmax(movie))(hidden) += negativeHiddenStates(hidden)
        }

        // compute error
        var predict = 0.0D
        for (r <- 0 until softmax) {
          if(RBM.negativeVisibleProbs.get(movie, r).isNaN){
            RBM.negativeVisibleProbs.set(0.0, movie, r)
          }
          predict += r * RBM.negativeVisibleProbs.get(movie, r)
        }
        nrmse += (rate - predict)*(rate-predict)
      }
    }

    val gradWeight = new ThreeDimensionArray(nVisible, softmax, nHidden)
    val gradVbias = new TwoDimensionArray(nVisible, softmax)
    val gradHbias = new OneDimensionArray(nHidden)

    for(movie <-0 until nVisible){
      for(hidden <- 0 until nHidden){
        for(rate <- 0 until softmax){
//          val CDp = CDpositive(movie)(rate)(hidden)/batchSize
          val CDp = RBM.CDpositive.get(movie, rate, hidden) /batchSize
//          val CDn = CDnegative(movie)(rate)(hidden)/batchSize
          val CDn = RBM.CDnegative.get(movie, rate, hidden) /batchSize

          RBM.CDinc.set(rbmParam.momentum * RBM.CDinc.get(movie, rate, hidden) + rbmParam.epsilonW*(CDp - CDn) - rbmParam.weightCost * param.getWeight(movie, rate, hidden),
            movie, rate, hidden)
          gradWeight.set(RBM.CDinc.get(movie,rate, hidden), movie,rate,hidden)
//          param.addWeight(RBM.CDinc.get(movie,rate, hidden), movie, rate, hidden)
        }
      }
      for(rate <- 0 until softmax) {
        if (RBM.positiveVisibleActivate.get(movie,rate) != 0.0 || RBM.negativeVisibleActivation.get(movie,rate) != 0.0) {
          RBM.positiveVisibleActivate.set(RBM.positiveVisibleActivate.get(movie, rate) /batchSize.toDouble, movie, rate)
          RBM.negativeVisibleActivation.set(RBM.negativeVisibleActivation.get(movie,rate) /batchSize.toDouble, movie, rate)
//          gradVbias(movie)(rate) = positiveVisibleActivate(movie)(rate) - negativeVisibleActivation(movie)(rate)
          RBM.vbiasInc.set(rbmParam.momentum*RBM.vbiasInc.get(movie, rate) + rbmParam.epsilonVb * (RBM.positiveVisibleActivate.get(movie,rate) - RBM.negativeVisibleActivation.get(movie,rate)), movie, rate)
          gradVbias.set(RBM.vbiasInc.get(movie, rate), movie, rate)
//          param.addVbias(RBM.vbiasInc.get(movie, rate), movie, rate)

//          param.vbias(movie)(rate) += learningRate * (positiveVisibleActivate(movie)(rate) - negativeVisibleActivation(movie)(rate))
        }
      }
    }
    for(hidden <- 0 until nHidden){
      if(RBM.positiveHiddenActivation.get(hidden) != 0.0 || RBM.negativeHiddenActivate.get(hidden) != 0.0){
        RBM.positiveHiddenActivation.set(RBM.positiveHiddenActivation.get(hidden) /batchSize.toDouble, hidden)

        RBM.negativeHiddenActivate.set(RBM.negativeHiddenActivate.get(hidden)/batchSize.toDouble, hidden)
//        negativeHiddenActivate(hidden) /= batchSize.toDouble
//        gradHbias(hidden) = positiveHiddenActivation(hidden) - negativeHiddenActivate(hidden)
//        param.hbias(hidden) += learningRate*(positiveHiddenActivation(hidden) - negativeHiddenActivate(hidden))
        RBM.hbiasInc.set(rbmParam.momentum*RBM.hbiasInc.get(hidden) + rbmParam.epsilonHb*(RBM.positiveHiddenActivation.get(hidden) - RBM.negativeHiddenActivate.get(hidden)), hidden)
        gradHbias.set(RBM.hbiasInc.get(hidden), hidden)
//        param.addHbias(RBM.hbiasInc.get(hidden), hidden)
      }
    }
    zeros()
//    (gradWeight, gradVbias, gradVbias, batchSize, nrmse / tc)
    logInfo("Square error at iteration: " + iteration + ": " + (nrmse : Double))
    (gradWeight, gradVbias, gradHbias, tc, nrmse)
//    (param, batchSize, nrmse /tc)
  }

  def movierating(a : Int): (Int, Int) = {
    (a / 10, a % 10)
  }

  def sigmoid(x: Double) = 1.0 / (1.0+math.exp(-x))


  def mergeParam(model: Params, newParam: Params, numReplica: Int): Unit ={
    for(movie <-0 until nVisible){
      for(rate <- 0 until softmax){
        for(hidden<-0 until nHidden)
          model.weight(movie)(rate)(hidden) = (model.weight(movie)(rate)(hidden) + newParam.weight(movie)(rate)(hidden)) /numReplica
        model.vbias(movie)(rate) = (model.vbias(movie)(rate) + newParam.vbias(movie)(rate)) /numReplica
      }
    }
    for(hidden <- 0 until nHidden){
      model.hbias(hidden) = (model.hbias(hidden) + newParam.hbias(hidden))/numReplica
    }
  }

  object RBM {
    val sumW = new OneDimensionArray(nHidden)
    val positiveHiddenActivation = new OneDimensionArray(nHidden)
    val positiveHiddenStates = Array.ofDim[Char](nHidden)
    val negativeHiddenStates = Array.ofDim[Char](nHidden)
    val negativeHiddenActivate = new OneDimensionArray(nHidden)
    val CDpositive = new ThreeDimensionArray(nVisible, softmax, nHidden)
    //  val CDpositive = Array.ofDim[Double](nVisible, softmax, nHidden)
    val CDnegative = new ThreeDimensionArray(nVisible, softmax,nHidden)
    val CDinc = new ThreeDimensionArray(nVisible, softmax, nHidden)
    val hbiasInc = new OneDimensionArray(nHidden)
    val vbiasInc = new TwoDimensionArray(nVisible, softmax)
    //  val CDnegative = Array.ofDim[Double](nVisible, softmax, nHidden)

//    private static char[] curposhidstates = new char[DataInfo.totalFeatures];
    val currentPositiveHiddenStatas = Array.ofDim[Char](nHidden)

    //  val positiveVisibleActivate = Array.ofDim[Double](nVisible, softmax)
    val positiveVisibleActivate = new TwoDimensionArray(nVisible, softmax)
    val negativeVisibleSoftmax = Array.ofDim[Char](nVisible)
    //  val negativeVisibleActivation = Array.ofDim[Double](nVisible, softmax)\
    val negativeVisibleActivation = new TwoDimensionArray(nVisible, softmax)
    //  val negativeVisibleProbs = Array.ofDim[Double](nVisible, softmax)
    val negativeVisibleProbs = new TwoDimensionArray(nVisible, softmax)
  }

  def zeros(): Unit ={
    RBM.CDpositive.zeroItself()
    RBM.CDnegative.zeroItself()
    RBM.positiveHiddenActivation.zeroItself()
    RBM.negativeHiddenActivate.zeroItself()
    RBM.positiveVisibleActivate.zeroItself()
    RBM.negativeVisibleActivation.zeroItself()

  }

//  def evaluate(test: Array[Array[Int]], param: Param) ={
//
//    val numTest = test.length
//
//    for(i<- 0 until numTest){
//      val num = test(i).length
//
//    }
//    ???
//  }

}



//object GradientDescent extends Logging {
//  def runMiniBatchSGD(
//                       data: RDD[Array[Int]],
//                       stepSize: Double,
//                       numIterations: Int,
//                       miniBatchFraction: Double,
//                       gradient: RBMGradient,
//                       params: Params,
//                       nVisible: Int,
//                       nHidden: Int,
//                       softmax: Int
//                       ) = {
//    val stochasticLossHistory = new ArrayBuffer[Double](numIterations)
//
//    val numExamples = data.count()
//
//    if (numExamples * miniBatchFraction < 1) {
//      logWarning("The miniBatchFraction is too small")
//    }
//
//    for (i <- 1 to numIterations) {
//      val bcParam = data.context.broadcast(params)
//
//      val (gradienSum, lossSum, miniBatchSize) = data.sample(false, miniBatchFraction, 42 + i)
//        .mapPartitions(t => Iterator(t))
//        .aggregate((Params.zeros(nVisible, nHidden, softmax), 0.0, 0L))(
//          seqOp = (c, v)=>{
//            println("Iterator length: "+v.length)
//            val l = gradient.compute(v, params, c._1)
//            (c._1, c._2 + l._2, c._3 + l._1 )
//          },
//          combOp = (c1, c2) =>{
//            c1._1.addi(c2._1, gradient.nVisible, gradient.nHidden, gradient.softmax)
//            (c1._1, c1._2 + c2._2, c1._3 + c2._3)
//          })
//      if(miniBatchSize > 0){
//        stochasticLossHistory.append(lossSum /miniBatchSize)
//        gradienSum.scale(-stepSize.toDouble/ miniBatchSize, nVisible, nHidden, softmax)
//
//        params.addi(gradienSum, nVisible, nHidden, softmax)
//      }
//      else {
//        logWarning(s"Iteration ($i/$numIterations). The size of sampled batch is zero")
//      }
//    }
//
//    logInfo("GradientDescent.runMiniBatchSGD finished. Last 10 stochastic losses %s".format(
//      stochasticLossHistory.takeRight(10).mkString(", ")))
//
//    (params, stochasticLossHistory.toArray)
//
//  }
//}
//
//class RBMGradient(val nVisible: Int, val nHidden: Int, val softmax: Int, val batchSize: Int){
//
//  def compute(iter: Iterator[Array[Int]], params: Params, cumParams:Params) ={
//    var loss = 0D
//    var count = 0L
//    val rbm = new RBM(nVisible, nHidden, softmax)
//    iter.grouped(batchSize).foreach{ seq =>
//      val numCol = seq.size
//      var (gradWeight, gradVbias, gradHBias, batchSize, rmse) = rbm.learn(seq.toList, 3)
//      loss += rmse
//      count += numCol
//    }
//    (count, loss)
//  }
//}
//
//object RBM{
//  def runSGD(data: RDD[Array[Int]],
//             rbm: RBM,
//             batchSize: Int,
//             maxNumIterations: Int,
//             fraction: Double,
//             learningRate: Double
////             ,
////             weightCost: Double,
////             rho: Double,
////             epsilon: Double
//              ) ={
//    val numVisible = rbm.nVisible
//    val numHidden = rbm.nHidden
//    val numSoftmax = rbm.softmax
//
//    val gradient = new RBMGradient(numVisible, numHidden, numSoftmax, batchSize)
//
//    data.persist(StorageLevel.MEMORY_AND_DISK).setName("RBM-dataBatch")
//    val (weights, loss) = GradientDescent.runMiniBatchSGD(data,
//      learningRate,
//      maxNumIterations,
//      fraction,
//      gradient,
//      rbm.param,
//      numVisible,
//      numHidden,
//      numSoftmax)
//    data.unpersist()
//    rbm
//  }


//}

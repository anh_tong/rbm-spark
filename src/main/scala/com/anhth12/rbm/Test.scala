package com.anhth12.rbm

import breeze.linalg.{DenseMatrix => BDM, DenseVector => BDV, SparseVector => BSV, Vector => BV}
import com.anhth12.param.Param


/**
 * Created by Tong Hoang Anh on 3/19/2015.
 */
object Test{

//  val noRBM = Runtime.getRuntime.freeMemory()
//  val rbm = new RBM(17770, 5, 100)
//val javaParam = new com.anhth12.param.Param(17770, 100, 5)
//  val dup = javaParam.duplicate()
//  val element = javaParam.getWeight(11780,2,64);
//  println(element)
//  val vBias = Array.fill[Double](17770, 5, 100)(1)
//  print((Runtime.getRuntime.freeMemory() - noRBM) + "bytes")
//  println (vBias.length)


//  val (gradWeight, _, _, _, rmse) =  rbm.learn(input, 2)

//  println(rmse)



  def main(args: Array[String]) {
//    val rbm = new RBM(6, 3, 2)
//
//    val input = List(Array(0,11,21), Array(0, 31), Array(0,11,21),Array(21,31,41), Array(21, 41), Array(21,31,41))
//    val javaParam = new com.anhth12.param.Param(6, 3, 2)
//
//    rbm.RBM.CDinc.zeroItself()
//    rbm.RBM.hbiasInc.zeroItself()
//    rbm.RBM.vbiasInc.zeroItself()
//    val a1 =  rbm.learn(input, 2, javaParam, new RBMParam, 1)
//    println(a1._3)
//    rbm.RBM.CDinc.zeroItself()
//    rbm.RBM.hbiasInc.zeroItself()
//    rbm.RBM.vbiasInc.zeroItself()
//    val a2 =  rbm.learn(input, 2, javaParam, new RBMParam, 1)
//    println(a2._3)
//
//    rbm.RBM.CDinc.zeroItself()
//    rbm.RBM.hbiasInc.zeroItself()
//    rbm.RBM.vbiasInc.zeroItself()
//    val a3 =  rbm.learn(input, 2, javaParam, new RBMParam, 1)
//    println(a3._3)
//
//    rbm.RBM.CDinc.zeroItself()
//    rbm.RBM.hbiasInc.zeroItself()
//    rbm.RBM.vbiasInc.zeroItself()
//    val a4 =  rbm.learn(input, 2, javaParam, new RBMParam, 1)
//    println(a4._3)
//    println(a4._2)
val numPartion: Int = 7

    val param = new Param(1,1,1)
    param.initWeight()
    val newParam = new Param(1,1,1)
    newParam.initWeight()

    println("param: " + param.weight()(0)(0)(0))
    println("newParam: " + newParam.weight()(0)(0)(0))

    newParam.subi(param).scale(1.0/numPartion)

    println("sub: " + newParam.weight()(0)(0)(0))

//    val scale = sub.scale(1.0 /numPartion)
//    println("scale: " + scale.weight()(0)(0)(0))
  }

}

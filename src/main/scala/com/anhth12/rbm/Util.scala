package com.anhth12.rbm

/**
 * Created by Tong Hoang Anh on 3/20/2015.
 */
object Util {

  def initWeight(numItem: Int, numFeatures: Int, softmax: Int) ={
    val ret = Array.ofDim[Double](numItem, softmax, numFeatures)
    for(i <- 0 until numItem)
      for(j <- 0 until numFeatures)
        for(k <- 0 until softmax)
          ret(i)(k)(j) = 0.02* scala.util.Random.nextDouble() + 0.01
    ret
  }

  def initVBias(numItem: Int, softmax: Int) = {
    Array.ofDim[Double](numItem, softmax)
  }

  def initHBias(numFeatures: Int) = {
    Array.ofDim[Double](numFeatures)
  }


}

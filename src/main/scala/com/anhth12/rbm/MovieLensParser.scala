package com.anhth12.rbm

import com.anhth12.rbm.Parser._
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext._

/**
 * Created by Tong Hoang Anh on 3/30/2015.
 */
object MovieLensParser {

  def loadData(sc: SparkContext, hadoopFile: String): RDD[(Int, Int)] ={
    sc.textFile(hadoopFile, 64).map(s => {
      val splitted = s.split("::")
      val movieId = splitted(1).toInt - 1
      val rate = (splitted(2).toDouble*2).toInt - 1
      (splitted(0).toInt, movieId*10 + rate )
    })
  }

  def groupByUser(input: RDD[(Int, Int)], numPartition: Int):RDD[(Int, Iterable[Int])] = {
    return input.groupByKey(numPartition)
  }

  def trainAndTest1(hadoopFile: String, probe: RDD[(Int, Int)], sc: SparkContext) = {
    val input = sc.wholeTextFiles(hadoopFile, 64).map(_._2).flatMap {
      file =>
        val splitted = file.split("\n")
        var flag = true
        var first = ""
        val map = splitted.map {
          line =>
            if (flag) {
              first = line
              flag = false
            }
            (first.substring(0, first.length - 1).toInt, line)
        }
        map
    }.filter(a => !a._2.endsWith(":"))
      .map(pair => {
      val splitted = pair._2.split(",")
      //user, movie, rating
      ((splitted(0).toInt, pair._1), splitted(1).toInt)
    })
    val probeRate = probe.map(a =>(a, 0))

    val testJoin = input.join(probeRate)
    val train = input.subtractByKey(probeRate)
    val test = input.subtract(train)
    (train, test)
  }

  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Netflix-Parser")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.kryoserializer.buffer.mb", "100")

    val sc = new SparkContext(conf)

    val probe = loadProbe("/root/word2vec/probe/probe.txt")

    val probeRDD = sc.parallelize(probe, 128)

    print(probeRDD.count())
    val (train, test)= trainAndTest1("/user/root/nflx/training_set", probeRDD, sc)
    train.map(a => a._1._1 + " " + a._1._2 + " " + a._2).saveAsTextFile("/user/root/nflx/train.probe")
    test.map(a => a._1._1 + " " + a._1._2 + " " + a._2).saveAsTextFile("/user/root/nflx/test.probe")  }
}

package com.anhth12.rbm

import com.anhth12.param.Param
import com.anhth12.rbm.JavaRBM
import com.anhth12.rbm.Parser._
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{Logging, SparkConf, SparkContext}
import java.co

import scala.util.Random

/**
 * Created by Tong Hoang Anh on 3/16/2015.
 */
object Main extends Logging {

  def main(args: Array[String]) {
    val Array(directory, numIterationsStr, miniBatchFractionStr) = args

    val jarFile = getClass.getProtectionDomain.getCodeSource.getLocation.getPath

    val conf = new SparkConf().setAppName("RBM").setJars(Array(jarFile))
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.kryoserializer.buffer.mb", "100")

    val numPartitions = 7

    val numIteration = numIterationsStr.toInt
    val miniBatchFraction = miniBatchFractionStr.toDouble

    val nVisible = 17770
    val sofmax = 5
    val nHidden = 100

    val sc = new SparkContext(conf)

    var javaParam = new com.anhth12.param.Param(17770, 100, 5)

    def doFirstWhenYouDoNotHaveData(directory: String) = {
      val rawData = parseRaw(directory, sc)
      val trainAndTest = rawData.randomSplit(Array(0.8, 0.2), Random.nextLong())
      val train = groupByUser(trainAndTest(0), 64)
      val test = groupByUser(trainAndTest(1), 64)
      saveRefinedData(train, "/user/root/netflix/train")
      test.join(train).map(a => a._2._1.mkString(",") + "#" + a._2._2.mkString(",")).saveAsTextFile("/user/root/netflix/test-join-train")
      saveRefinedData(test, "/user/root/netflix/test")
    }

    //    doFirstWhenYouDoNotHaveData()

    val trainData = loadData(sc, "/user/root/netflix/train.probe")
    //    val testData = loadData(sc, "/user/root/netflix/test").join(trainData)
    //      .map(a => a._2._1.mkString(",") + "#" + a._2._2.mkString(","))
    //      .saveAsTextFile("/user/root/netflix/test-join-train")
    val testJoinTrain = loadTestJoinTrain(sc, "/user/root/netflix/test-join-train.probe").setName("test-join-train")
    testJoinTrain.cache()
    testJoinTrain.count()

    val movierate1 = trainData.map(_._2).flatMap(a => a.map(t => t)).map(t => (t, 1)).reduceByKey(_ + _)

    val collectMovieRate = movierate1.collectAsMap().map(a => (a._1: Integer, a._2: Integer))
    javaParam.initWeight()
    import collection.JavaConversions._
    javaParam.initVbias(collectMovieRate)

    val repartitioned = trainData.map(_._2).repartition(numPartitions).persist(StorageLevel.MEMORY_AND_DISK).setName("data-after-partition")
    var start = System.currentTimeMillis();
    for (iteration <- 0 until numIteration) {
      val javaParamBC = sc.broadcast(javaParam)
      val result = repartitioned
        .sample(false, miniBatchFraction, 42 + iteration)
        .mapPartitions(
          t =>
            Iterator(t)
        )
        .map(t => {
        JavaRBM.nVisbile = nVisible
        JavaRBM.nHidden = nHidden
        JavaRBM.softmax = sofmax
        val javaRBM = JavaRBM.getInstance()

        javaRBM.setParam(javaParamBC.value)
        javaRBM.train(t.toList, iteration)
      })
      val collectedResult = result.collect()

      collectedResult.foreach(newParam => {
        javaParam.addi(newParam)
      })

      if (iteration % 5 == 0) {
        val (nrmse, numTest) = testJoinTrain.map(a => JavaRBM.evaluate(a._1, a._2, javaParam, nVisible, nHidden, sofmax))
          .aggregate((0.0D, 0))(
            seqOp = (c, v) => {
              (c._1 + v._1, c._2 + v._2)
            },
            combOp = (c1, c2) => {
              (c1._1 + c2._1, c1._2 + c2._2)
            }
          )
        var end = System.currentTimeMillis()
        println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
        printToFile(new java.io.File(iteration.toString + ".txt")) {
          p => p.println("iteration " + iteration + " loss: " + math.sqrt(nrmse / numTest) + " time elapsed: " + (end - start) + " ms")
        }
        start = System.currentTimeMillis()
      }


    }

    def zeroValue() = new Param(nVisible, nHidden, sofmax)

  }

  def saveToTextFile(dat: RDD[Array[Int]], hadoopFile: String) = {
    dat.map(a => a.mkString(",")).saveAsTextFile(hadoopFile)
  }

  def load(context: SparkContext, hadoopFile: String) = {
    context.wholeTextFiles(hadoopFile, 8).map(_._2)
      .flatMap(s => s.split("\n")).map(s => s.split(",").map(a => a.toInt))
  }

  def getMovieRating(input: Int): (Int, Int) = {
    (input / 10, input % 10)
  }

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try {
      op(p)
    } finally {
      p.close()
    }
  }





}

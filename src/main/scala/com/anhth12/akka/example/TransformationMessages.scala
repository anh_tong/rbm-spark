package com.anhth12.akka.example

/**
 * Created by Tong Hoang Anh on 4/2/2015.
 */
  final case class TransformationJob(text: String)
  final case class TransformationResult(text: String)
  final case class JobFailed(reason: String, job: TransformationJob)
  case object BackendRegistration


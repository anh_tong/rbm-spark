package com.anhth12.akka.example

import language.postfixOps
import _root_.akka.actor.Actor
import _root_.akka.actor.ActorSystem
import _root_.akka.actor.Props
import _root_.akka.actor.RootActorPath
import _root_.akka.cluster.Cluster
import _root_.akka.cluster.ClusterEvent.CurrentClusterState
import _root_.akka.cluster.ClusterEvent.MemberUp
import _root_.akka.cluster.Member
import _root_.akka.cluster.MemberStatus
import _root_.com.typesafe.config.ConfigFactory

/**
 * Created by Tong Hoang Anh on 4/2/2015.
 */
class TransformationBackend extends Actor {

  val cluster = Cluster(context.system)

  // subscribe to cluster changes, MemberUp
  // re-subscribe when restart
  override def preStart(): Unit = cluster.subscribe(self, classOf[MemberUp])
  override def postStop(): Unit = cluster.unsubscribe(self)

  def receive = {
    case TransformationJob(text) => sender ! TransformationResult(text.toUpperCase)
    case state: CurrentClusterState =>
      state.members.filter(_.status == MemberStatus.Up) foreach register
    case MemberUp(m) => register(m)
  }

  def register(member: Member): Unit =
    if (member.hasRole("frontend"))
      context.actorSelection(RootActorPath(member.address) / "user" / "frontend") !
        BackendRegistration
}
//#backend

object TransformationBackend {
  def main(args: Array[String]): Unit = {
    // Override the configuration of the port when specified as program argument
    val port = if (args.isEmpty) "0" else args(0)
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [backend]")).
      withFallback(ConfigFactory.load())

    val system = ActorSystem("ClusterSystem", config)
    system.actorOf(Props[TransformationBackend], name = "backend")
  }
}

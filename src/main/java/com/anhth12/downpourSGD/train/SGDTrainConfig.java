/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.train;

import java.io.Serializable;
import org.apache.spark.storage.StorageLevel;

/**
 *
 * @author Administrator
 */
public class SGDTrainConfig implements Serializable{

    private static final long serialVersionUID = 1L;

    //number of model replica
    public int numModelReplica = 1;

    public StorageLevel storageLevel = StorageLevel.MEMORY_ONLY();

    public int epoch = 500;

    public double minLoss = 0.1;

    public double learningRate = 0.001;

    /**
     * regulation
     */
    public boolean useRegulation = false;
    public double lambda1 = 0;
    public double lambda2 = 0.0001;

    /**
     * calculate loss function
     */
    public int lossCalStep = 1;
    public boolean printLoss = false;
    
    /**
     * parameter output during each epoch of training
     */
    public boolean paramOuput = false;
    public int paramOutputStep = 5;
    public String paramOutputPath = null;
    
    /**
     * for AutoEncoder denoising
     */
    public boolean doCorruption = true;
    public double corruptionLevel = 0.3;
    
    /**
     * for RBM fast CD1 algorithm
     */
    public boolean useHitonCD1 = true;
    
    /**
     * use Conjugate Gradient on front step
     */
    public boolean useCG = false;
    
    public int cgEpochStep = 1;
    
    // max iteration time in one time cg
    public int cgMaxIterations = 100;

    // the cg min gradient update tolerance
    public double cgTolerance = 0.0001;

    // cg init step size
    public double cgInitStepSize = 0.01;
    
}

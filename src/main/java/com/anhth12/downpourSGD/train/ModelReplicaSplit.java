/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.train;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

/**
 *
 * @author Administrator
 * @param <T>
 */
public class ModelReplicaSplit<T> implements Serializable{
    
    private final Random rand = new Random(System.currentTimeMillis());
    
    public JavaPairRDD<Integer, Iterable<T>> split(JavaRDD<T> input, int numReplica, SGDTrainConfig config){
        JavaPairRDD<Integer, Iterable<T>> output = input.mapToPair(new SplitModelReplica(numReplica))
                .groupByKey()
                .persist(config.storageLevel);
        output.count();
        return output;
    }
    
    private class SplitModelReplica implements PairFunction<T, Integer, T>{
        
        int numReplica;

        public SplitModelReplica(int numReplica) {
            this.numReplica = numReplica;
        }
        
        @Override
        public Tuple2<Integer, T> call(T t) throws Exception {
            int idx = rand.nextInt(numReplica);
            return new Tuple2<>(idx, t);
        }
        
    }
    
}

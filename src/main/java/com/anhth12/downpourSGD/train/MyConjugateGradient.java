/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.train;

import cc.mallet.optimize.BackTrackLineSearch;
import cc.mallet.optimize.LineOptimizer;
import cc.mallet.optimize.Optimizable;
import cc.mallet.optimize.Optimizer;
import cc.mallet.optimize.OptimizerEvaluator;
import cc.mallet.types.MatrixOps;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class MyConjugateGradient implements Optimizer {
    
    private static final Logger logger = Logger.getLogger(MyConjugateGradient.class);

    boolean converged = false;
    Optimizable.ByGradientValue optimizable;
    LineOptimizer.ByGradient lineMaximizer;

    double initialStepSize = 1;
    double tolerance = 0.0001;
    double gradientTolerance = 0.001;
    int maxIterations = 1000;
    private String myName = "";

    final double eps = 1.0e-10;
    private OptimizerEvaluator.ByGradient eval;

    public MyConjugateGradient(Optimizable.ByGradientValue optimizable, double initialStepSize) {
        this.optimizable = optimizable;
        this.initialStepSize = initialStepSize;
        lineMaximizer = new BackTrackLineSearch(optimizable);
    }

    public MyConjugateGradient(Optimizable.ByGradientValue optimizable) {
        this(optimizable, 0.01);
    }

    @Override
    public Optimizable getOptimizable() {
        return this.optimizable;
    }

    @Override
    public boolean isConverged() {
        return converged;
    }

    public void setTolerance(double tolerance) {
        this.tolerance = tolerance;
    }

    public void setEvaluator(OptimizerEvaluator.ByGradient eval) {
        this.eval = eval;
    }

    public void setLineMaximizer(LineOptimizer.ByGradient lineMaximizer) {
        this.lineMaximizer = lineMaximizer;
    }

    public void setInitialStepSize(double initialStepSize) {
        this.initialStepSize = initialStepSize;
    }

    public double getInitialStepSize() {
        return this.initialStepSize;
    }

    public double getStepSize() {
        return step;
    }

    // The state of a conjugate gradient search
    double fp, gg, gam, dgg, step, fret;
    double[] xi, g, h;
    int j, iterations;

    @Override
    public boolean optimize() {
        return optimize(maxIterations);
    }

    @Override
    public boolean optimize(int i) {
        myName = Thread.currentThread().getName();
        if (converged) {
            return true;
        }
        int n = optimizable.getNumParameters();
        long last = System.currentTimeMillis();
        if (xi == null) {
            fp = optimizable.getValue();
            xi = new double[n];
            g = new double[n];
            h = new double[n];
            optimizable.getValueGradient(xi);
            System.arraycopy(xi, 0, g, 0, n);
            System.arraycopy(xi, 0, h, 0, n);
            step = initialStepSize;
            iterations = 0;
        }

        long curr = 0;
        for (int iterationCount = 0; iterationCount < i; iterationCount++) {
            curr = System.currentTimeMillis();
            logger.info(myName + " ConjugateGradient: At iteration " + iterations + ", cost = " + fp + " -"
                    + (curr - last));
            last = curr;
            try {
                step = lineMaximizer.optimize(xi, step);
            } catch (Throwable e) {
                logger.info(e.getMessage());
            }
            fret = optimizable.getValue();
            optimizable.getValueGradient(xi);

            // This termination provided by "Numeric Recipes in C".
            if ((0 < tolerance) && (2.0 * Math.abs(fret - fp) <= tolerance * (Math.abs(fret) + Math.abs(fp) + eps))) {
                logger.info("ConjugateGradient converged: old value= " + fp + " new value= " + fret + " tolerance="
                        + tolerance);
                converged = true;
                return true;
            }
            fp = fret;

            // This termination provided by McCallum
            double twoNorm = MatrixOps.twoNorm(xi);
            if (twoNorm < gradientTolerance) {
                logger.info("ConjugateGradient converged: gradient two norm " + twoNorm + ", less than "
                        + gradientTolerance);
                converged = true;
                return true;
            }

            dgg = gg = 0.0;
            for (j = 0; j < xi.length; j++) {
                gg += g[j] * g[j];
                dgg += xi[j] * (xi[j] - g[j]);
            }
            gam = dgg / gg;

            for (j = 0; j < xi.length; j++) {
                g[j] = xi[j];
                h[j] = xi[j] + gam * h[j];
            }
            assert (!MatrixOps.isNaN(h));

            // gdruck
            // Mallet line search algorithms stop search whenever
            // a step is found that increases the value significantly.
            // ConjugateGradient assumes that line maximization finds something
            // close
            // to the maximum in that direction. In tests, sometimes the
            // direction suggested by CG was downhill. Consequently, here I am
            // setting the search direction to the gradient if the slope is
            // negative or 0.
            if (MatrixOps.dotProduct(xi, h) > 0) {
                MatrixOps.set(xi, h);
            } else {
                logger.warn("Reverting back to GA");
                MatrixOps.set(h, xi);
            }

            iterations++;
            if (iterations > maxIterations) {
                logger.info("Too many iterations in ConjugateGradient.java");
                converged = true;
                return true;
            }

            if (eval != null) {
                eval.evaluate(optimizable, iterations);
            }
        }
        return false;
    }

    public void reset() {
        xi = null;
    }

}

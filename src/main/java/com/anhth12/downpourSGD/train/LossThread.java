/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.train;

import java.util.List;

/**
 *
 * @author Administrator
 */
final class LossThread implements Runnable {
    private SGDBase sgd;
    private List<SampleVector> samples;
    private boolean running = false;
    private double error = 0;

    public LossThread(SGDBase _sgd) {
        this.sgd = _sgd;
    }

    public double getError() {
        return this.error;
    }

    public boolean isRunning() {
        return this.running;
    }

    public void sumLoss(List<SampleVector> xy) {
        this.samples = xy;
        this.error = 0;
        new Thread(this).start();
    }

    @Override
    public void run() {
        this.running = true;
        this.error = this.sgd.lost(this.samples);
        this.running = false;
    }
}

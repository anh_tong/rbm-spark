package com.anhth12.downpourSGD.train;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class SGDPesistableWrite {

    private static final Logger logger = Logger.getLogger(SGDPesistableWrite.class);

    public static void output(String path, SGDPersistable sgd) {
        DataOutputStream dos = null;
        try {
            dos = new DataOutputStream(new FileOutputStream(path));
            sgd.write(dos);
        } catch (Exception e) {
            logger.error("Write SGD failed", e);
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (Exception e) {
                }
            }
        }
    }
}

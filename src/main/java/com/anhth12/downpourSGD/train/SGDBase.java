/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.train;

import java.io.Serializable;
import java.util.List;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Administrator
 */
public abstract class SGDBase implements SGDPersistable, Serializable{
    
    private static final long serialVersionUID = 1L;
    
    protected SGDParam param;
    
    protected abstract void gradientUpdateMiniBatch(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam);
    
    protected abstract void gradientUpdateCG(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam);
    
    protected abstract void mergeParam(SGDParam param, int numModelReplica);
    
    protected abstract double lost(List<SampleVector> samples);
    
    protected abstract boolean isSupervised();
    
    public final SGDParam getParam(){
        return param;
    }
    
    
    
}

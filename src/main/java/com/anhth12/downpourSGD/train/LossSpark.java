/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.train;

import com.clearspring.analytics.util.Lists;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

/**
 *
 * @author Administrator
 */
public final class LossSpark implements Function<Tuple2<Integer, Iterable<SampleVector>>, Double>{
    
    private SGDBase sgd;

    public LossSpark(SGDBase sgd) {
        this.sgd = sgd;
    }

    @Override
    public Double call(Tuple2<Integer, Iterable<SampleVector>> arg0) throws Exception {
        return this.sgd.lost(Lists.newArrayList(arg0._2()));
    }
}

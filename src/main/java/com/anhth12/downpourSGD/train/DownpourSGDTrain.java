/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.train;

import java.io.Serializable;
import java.util.*;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.*;

/**
 *
 * @author Administrator
 */
public class DownpourSGDTrain implements Serializable{

    public static void train(SGDBase sgd, JavaPairRDD<Integer, Iterable<org.apache.spark.mllib.linalg.Vector>> rdd, SGDTrainConfig config, int numSample){
        for (int epoch = 0; epoch < config.epoch; epoch ++){

        }

    }
    
    public static void train(SGDBase sgd, JavaRDD<SampleVector> rdd, SGDTrainConfig config){
        long numSample = rdd.count();
        
        int numModelReplica = config.numModelReplica;
        
        ModelReplicaSplit<SampleVector> split = new ModelReplicaSplit<>();
        JavaPairRDD<Integer, Iterable<SampleVector>> modedSplit = split.split(rdd, numModelReplica, config);
        
        for (int epoch = 0; epoch < config.epoch; epoch++) {
            JavaRDD<DeltaSpark> deltas = modedSplit.map(new DeltaSpark(sgd, config, epoch));
            for (DeltaSpark delta: deltas.collect()) {
                sgd.mergeParam(delta.getParam(), numModelReplica);
            }
            
            if (config.paramOuput && (epoch % config.paramOutputStep) == 0) {
                SGDPesistableWrite.output(config.paramOutputPath, sgd);
            }
            
            if (!config.printLoss) {
                continue;
            }
            
            if ((epoch%config.lossCalStep) != 0) {
                continue;
            }
            
            List<Double> listLost = modedSplit.map(new LossSpark(sgd)).collect();
            double error = 0.0;
            for(Double d: listLost){
                error += d;
            }
            error /= numSample;
            if (error <= config.minLoss) {
                break;
            }
        }
    }
    
    /**
     * Standalone multiple threads train work
     * 
     * @param sgd The SGD node to be trained
     * @param samples The input supervise samples
     * @param config Specify the train configuration
     */
    public static void train(SGDBase sgd, List<SampleVector> samples, SGDTrainConfig config) {
        int xy_n = (int) samples.size();
        int nrModelReplica = config.numModelReplica;
        HashMap<Integer, List<SampleVector>> list_map = new HashMap<Integer, List<SampleVector>>();
        for (int i = 0; i < nrModelReplica; i++) {
        	list_map.put(i, new ArrayList<SampleVector>());
        }
        Random rand = new Random(System.currentTimeMillis());
        for (SampleVector v: samples) {
            int id = rand.nextInt(nrModelReplica);
            list_map.get(id).add(v);
        }
        
        List<DeltaThread> threads = new ArrayList<DeltaThread>();
        List<LossThread> loss_threads = new ArrayList<LossThread>();
        for (int i = 0; i < nrModelReplica; i++) {
            threads.add(new DeltaThread(sgd, config, list_map.get(i)));
            loss_threads.add(new LossThread(sgd));
        }

        // start iteration
        for (int epoch = 1; epoch <= config.epoch; epoch++) {
            // thread start
            for(DeltaThread thread : threads) {
            	thread.train(epoch);
            }

            // waiting for all stop
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                }
                boolean allStop = true;
                for(DeltaThread thread : threads) {
                    if (thread.isRunning()) {
                        allStop = false;
                        break;
                    }
                }
                if (allStop) {
                    break;
                }
            }

            // update
            for(DeltaThread thread : threads) {
            	sgd.mergeParam(thread.getParam(), nrModelReplica);
            }

            System.out.println("train done for this iteration-" + epoch);

            /**
             * 1 parameter output
             */
            if(config.paramOuput && (0 == (epoch % config.paramOutputStep))) {
            	SGDPesistableWrite.output(config.paramOutputPath, sgd);
            }
            
            /**
             * 2 loss print
             */
            if(!config.printLoss) {
            	continue;
            }
            if (0 != (epoch % config.lossCalStep)) {
                continue;
            }

            // sum loss
            for (int i = 0; i < nrModelReplica; i++) {
            	loss_threads.get(i).sumLoss(threads.get(i).getSamples());
            }

            // waiting for all stop
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                }
                boolean allStop = true;
                for(LossThread thread : loss_threads) {
                    if (thread.isRunning()) {
                        allStop = false;
                        break;
                    }
                }
                if (allStop) {
                    break;
                }
            }

            // sum up
            double totalError = 0;
            for(LossThread thread : loss_threads) {
                totalError += thread.getError();
            }
            totalError /= xy_n;
            System.out.println("iteration-" + epoch + " done, total error is " + totalError);
            if (totalError <= config.minLoss) {
                break;
            }
        }
    }
    
}

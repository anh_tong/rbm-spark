/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.train;

import com.anhth12.downpourSGD.util.MathUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.spark.api.java.function.Function;
import org.jblas.DoubleMatrix;
import scala.Tuple2;

/**
 *
 * @author Administrator
 */
public final class DeltaSpark implements Function<Tuple2<Integer, Iterable<SampleVector>>, DeltaSpark> {

    private static final long serialVersionUID = 1L;
    private SGDBase sgd;
    private SGDTrainConfig config;
    private SGDParam param;
    private int currentEpoch = 0;

    public DeltaSpark(SGDBase sgd, SGDTrainConfig config, int epoch) {
        this.sgd = sgd;
        this.config = config;
        this.currentEpoch = epoch;
        param = sgd.getParam().dup();
    }
    
    public SGDParam getParam(){
        return param;
    }

    @Override
    public DeltaSpark call(Tuple2<Integer, Iterable<SampleVector>> t1) throws Exception {
        List<SampleVector> myList = new ArrayList<SampleVector>();
        for (SampleVector v: t1._2()) {
            myList.add(v);
        }
        Collections.shuffle(myList);
        
        DoubleMatrix x_samples = MathUtil.convertX2Matrix(myList);
        DoubleMatrix y_samples = null;
        if(sgd.isSupervised()) {
        	y_samples = MathUtil.convertY2Matrix(myList);
        }

        // check whether we use cg this time
        if (config.useCG && (this.currentEpoch <= config.cgEpochStep)) {
        	this.sgd.gradientUpdateCG(config, x_samples, y_samples, param);
        } else {
        	this.sgd.gradientUpdateMiniBatch(config, x_samples, y_samples, param);
        }
        return this;
    }

}

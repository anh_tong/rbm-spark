/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.train;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author Administrator
 */
public interface SGDPersistable {
    
    public void read(DataInput in) throws IOException;
    
    public void write(DataOutput out) throws IOException;
    
    public void print(Writer wr) throws IOException;
    
}

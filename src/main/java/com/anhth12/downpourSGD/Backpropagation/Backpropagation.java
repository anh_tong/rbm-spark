/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.Backpropagation;

import cc.mallet.optimize.Optimizable;
import com.anhth12.downpourSGD.train.MyConjugateGradient;
import com.anhth12.downpourSGD.train.SGDBase;
import com.anhth12.downpourSGD.train.SGDParam;
import com.anhth12.downpourSGD.train.SGDTrainConfig;
import com.anhth12.downpourSGD.train.SampleVector;
import com.anhth12.downpourSGD.util.MathUtil;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import org.apache.log4j.Logger;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author anhth12
 */
public class Backpropagation extends SGDBase {

    private static final long versionSerialUID = 1L;
    private static final Logger logger = Logger.getLogger(Backpropagation.class);

    protected int in;
    protected int out;
    protected int[] hiddens;
    protected BackpropagationParam backpropagationParam;

    public Backpropagation(int in, int out, int[] hiddens, DoubleMatrix[] w, DoubleMatrix[] b) {
        this.in = in;
        this.out = out;
        this.hiddens = new int[hiddens.length];
        System.arraycopy(hiddens, 0, this.hiddens, 0, hiddens.length);
        backpropagationParam = new BackpropagationParam(in, out, hiddens, w, b);
        param = backpropagationParam;
    }

    public Backpropagation(int in, int out, int[] hidden) {
        this(in, out, hidden, null, null);
    }

    protected Backpropagation(int visible, int hidden, DoubleMatrix[] w, DoubleMatrix[] b) {
        this.in = visible;
        this.out = visible;
        this.hiddens = new int[1];
        this.hiddens[0] = hidden;
        backpropagationParam = new BackpropagationParam(in, out, hiddens, w, b);
        param = backpropagationParam;
    }

    protected Backpropagation(int visible, int hidden) {
        this(visible, hidden, null, null);
    }

    /**
     * Update Gradient with Mini Batch Strategy
     *
     * Feedforward first then back propagation
     *
     * @param config
     * @param xSample
     * @param ySample
     * @param currentParam
     */
    @Override
    protected void gradientUpdateMiniBatch(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam) {
        int numOfSamples = xSample.rows;
        BackpropagationParam bpParam = (BackpropagationParam) currentParam;

        DoubleMatrix[] activation = new DoubleMatrix[bpParam.numberOfLayer];
        DoubleMatrix[] lBias = new DoubleMatrix[bpParam.numberOfLayer];

        //feed forward
        activation[0] = xSample;
        for (int i = 1; i < bpParam.numberOfLayer; i++) {
            activation[i] = activation[i - 1]
                    .mmul(bpParam.getW()[i - 1].transpose())
                    .addiRowVector(bpParam.getB()[i - 1]);
            MathUtil.sigmoid(activation[i]);
        }

        //backward
        DoubleMatrix ai = activation[bpParam.getNumberOfLayer() - 1];
        lBias[bpParam.getNumberOfLayer() - 1] = ai.sub(ySample).muli(ai).mul(ai.neg().addi(1.0));

        for (int i = bpParam.getNumberOfLayer() - 2; i >= 1; i--) {
            ai = activation[i];
            lBias[i] = lBias[i + 1].mmul(bpParam.getW()[i]).muli(ai).muli(ai.neg().addi(1.0));
        }

        //delta
        for (int i = 0; i < bpParam.getW().length; i++) {
            DoubleMatrix deltaWi = lBias[i + 1].transpose().mmul(activation[i]).divi(numOfSamples);
            if (config.useRegulation) {
                if (config.lambda2 != 0) {
                    deltaWi.add(bpParam.getW()[i].mul(config.lambda2));
                }
            }
            bpParam.getW()[i].subi(deltaWi.muli(config.learningRate));
        }
        for (int i = 0; i < bpParam.getB().length; i++) {
            DoubleMatrix deltaBi = lBias[i + 1].columnSums().divi(numOfSamples);
            bpParam.getB()[i].subi(deltaBi.transpose().mul(config.learningRate));
        }

    }

    @Override
    protected void gradientUpdateCG(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam) {
        BackpropagationOptimizer optimizer = new BackpropagationOptimizer(config, xSample, ySample, (BackpropagationParam)currentParam);
        MyConjugateGradient cg = new MyConjugateGradient(optimizer, config.cgInitStepSize);
        cg.setTolerance(config.cgTolerance);
        try {
            cg.optimize(config.cgMaxIterations);
        } catch (Exception e) {
            logger.error("", e);
        }

    }

    @Override
    protected void mergeParam(SGDParam param, int numModelReplica) {
        BackpropagationParam newParam = (BackpropagationParam) param;
        for (int i = 0; i < backpropagationParam.getW().length; i++) {
            backpropagationParam.getW()[i]
                    .addi(newParam.getW()[i].sub(backpropagationParam.getW()[i])
                            .divi(numModelReplica));
        }
        for (int i = 0; i < backpropagationParam.getB().length; i++) {
            backpropagationParam.getB()[i]
                    .addi(newParam.getB()[i].sub(backpropagationParam.getB()[i])
                            .divi(numModelReplica));
        }
    }

    @Override
    protected double lost(List<SampleVector> samples) {
        DoubleMatrix xSamples = MathUtil.convertX2Matrix(samples);
        DoubleMatrix ySamples = MathUtil.convertY2Matrix(samples);
        DoubleMatrix sigmoidOutput = sigmoidOutput(xSamples);
        return MatrixFunctions.powi(sigmoidOutput.sub(ySamples), 2).sum();
    }

    public final DoubleMatrix sigmoidOutput(DoubleMatrix input) {
        DoubleMatrix output = input;
        for (int i = 0; i < backpropagationParam.getW().length; i++) {
            output = output.mmul(
                    backpropagationParam.getW()[i].transpose())
                    .addiRowVector(
                            backpropagationParam.getB()[i]
                    );
            MathUtil.sigmoid(output);
        }
        return output;
    }

    public final void sigmoidOutput(double[] input, double[] output) {
        DoubleMatrix inputMatrix = new DoubleMatrix(input).transpose();
        DoubleMatrix outputMatrix = sigmoidOutput(inputMatrix);
        for (int i = 0; i < output.length; i++) {
            output[i] = outputMatrix.get(0, i);
        }
    }

    @Override
    protected boolean isSupervised() {
        return true;
    }

    @Override
    public void read(DataInput in) throws IOException {
        this.in = in.readInt();
        this.out = in.readInt();
        for (int i = 0; i < hiddens.length; i++) {
            hiddens[i] = in.readInt();
        }
        for (int i = 0; i < hiddens.length; i++) {
            for (int row = 0; row < backpropagationParam.getW()[i].rows; row++) {
                for (int column = 0; column < backpropagationParam.getW()[i].columns; column++) {
                    backpropagationParam.getW()[i].put(row, column, in.readDouble());
                }
            }
            for (int row = 0; row < backpropagationParam.getB()[i].rows; row++) {
                for (int column = 0; column < backpropagationParam.getB()[i].columns; column++) {
                    backpropagationParam.getB()[i].put(row, column, in.readDouble());
                }
            }
        }
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(in);
        out.writeInt(this.out);
        for (int i = 0; i < hiddens.length; i++) {
            out.writeInt(hiddens[i]);
        }
        for (int i = 0; i < hiddens.length; i++) {
            for (int row = 0; row < backpropagationParam.getW()[i].rows; row++) {
                for (int column = 0; column < backpropagationParam.getW()[i].columns; column++) {
                    out.writeDouble(backpropagationParam.getW()[i].get(row, column));
                }
            }
            for (int row = 0; row < backpropagationParam.getB()[i].rows; row++) {
                for (int column = 0; column < backpropagationParam.getB()[i].columns; column++) {
                    out.writeDouble(backpropagationParam.getB()[i].get(row, column));
                }
            }
        }
    }

    @Override
    public void print(Writer wr) throws IOException {
        String newLine = System.getProperty("line.separator");
        wr.write(String.valueOf(this.in));
        wr.write(",");
        wr.write(String.valueOf(this.out));
        wr.write(newLine);
        for (int i = 0; i < hiddens.length; i++) {
            wr.write(String.valueOf(hiddens[i]));
            wr.write(",");
        }
        wr.write(newLine);
        for (int i = 0; i < hiddens.length; i++) {
            for (int j = 0; j < backpropagationParam.getW()[i].rows; j++) {
                for (int k = 0; k < backpropagationParam.getW()[i].columns; k++) {
                    wr.write(String.valueOf(backpropagationParam.getW()[i].get(j, k)));
                    wr.write(",");
                }
                wr.write(newLine);
            }
            wr.write(newLine);
            for (int j = 0; j < backpropagationParam.getB()[i].rows; j++) {
                for (int k = 0; k < backpropagationParam.getB()[i].columns; k++) {
                    wr.write(String.valueOf(backpropagationParam.getB()[i].get(j, k)));
                    wr.write(",");
                }
                wr.write(newLine);
            }
            wr.write(newLine);
        }
        wr.write(newLine);
    }

    protected class BackpropagationOptimizer implements Optimizable.ByGradientValue {

        protected int numberOfSamples;
        protected SGDTrainConfig config;
        protected DoubleMatrix xSamples;
        protected DoubleMatrix ySamples;
        protected BackpropagationParam bpParam;
        protected DoubleMatrix[] activation;

        /**
         * Constructor of Optimizer for backpropagation algorithm NOTE: first
         * activation layer will take <code>xSamples</code> as the first element
         *
         * @param config
         * @param xSamples
         * @param ySamples
         * @param currentParam
         */
        public BackpropagationOptimizer(SGDTrainConfig config, DoubleMatrix xSamples,
                DoubleMatrix ySamples, BackpropagationParam currentParam) {
            this.config = config;
            this.xSamples = xSamples;
            this.ySamples = ySamples;
            numberOfSamples = xSamples.getRows();
            this.bpParam = currentParam;
            activation = new DoubleMatrix[bpParam.getNumberOfLayer()];
            activation[0] = xSamples;
        }

        /**
         * Get Gradient back ward
         *
         * @param doubles
         */
        @Override
        public void getValueGradient(double[] doubles) {
            DoubleMatrix[] lBias = new DoubleMatrix[bpParam.getNumberOfLayer()];

            int outputLayer = bpParam.getNumberOfLayer() - 1;
            DoubleMatrix ai = activation[outputLayer];
            //(A_(i-1) - y)*A_(i-1)*(1- A_(i-1))
            lBias[outputLayer] = ai.sub(ySamples).muli(ai).muli(ai.neg().addi(1));

            for (int i = bpParam.getNumberOfLayer() - 2; i >= 1; i--) {
                ai = activation[i];
                //backward
                //lbias_i = lBias_(i+1)*W_i*A_i*(1 - A_i) = lBias_(i+1)*W_i*f'(z): f is sigmoid
                lBias[i] = lBias[i + 1].mmul(bpParam.getW()[i]).muli(ai).muli(ai.neg().addi(1.0));
            }

            int idx = 0;
            for (int i = 0; i < bpParam.getW().length; i++) {
                //Delta W i = lBiase_(i+1)'*A_i/n
                DoubleMatrix deltaWi = lBias[i + 1].transpose().mmul(activation[i]).divi(numberOfSamples);
                if (config.useRegulation) {
                    if (config.lambda2 != 0) {
                        //Delta W_i + W_i*lambda2
                        deltaWi.addi(bpParam.getW()[i].mul(config.lambda2));
                    }
                }
                for (int row = 0; row < deltaWi.rows; row++) {
                    for (int column = 0; column < deltaWi.columns; column++) {
                        doubles[idx++] = -deltaWi.get(row, column);
                    }
                }
            }
            for (int i = 0; i < bpParam.getB().length; i++) {
                DoubleMatrix deltaBi = lBias[i + 1].columnSums().divi(numberOfSamples);
                for (int row = 0; row < deltaBi.rows; row++) {
                    for (int column = 0; column < deltaBi.columns; column++) {
                        doubles[idx++] = -deltaBi.get(row, column);
                    }
                }
            }
        }

        /**
         * Feed forward calculate all activations in order to get final loss
         * value
         *
         * @return -loss
         */
        @Override
        public double getValue() {
            for (int i = 1; i < bpParam.getNumberOfLayer(); i++) {
                //A_i = A_(i-1)*W_(i-1)' + b_(i-1)
                activation[i] = activation[i - 1]
                        .mmul(bpParam.getW()[i - 1]
                                .transpose()).addiRowVector(bpParam.getB()[i - 1]);
                MathUtil.sigmoid(activation[i]);
            }
            //loss = (last activation - y)^2/number of sample
            double loss = MatrixFunctions.powi(
                    activation[bpParam.getNumberOfLayer() - 1].sub(ySamples),
                    2)
                    .sum()
                    / numberOfSamples;

            if (config.useRegulation) {
                //only lambda2 for Backpropagation
                if (config.lambda2 != 0) {
                    double sumSquares = 0;
                    for (int i = 0; i < bpParam.getW().length; i++) {
                        sumSquares += MatrixFunctions.pow(bpParam.getW()[i], 2).sum();
                    }
                    loss += 0.5 * config.lambda2 * sumSquares;
                }
            }
            return -loss;
        }

        /**
         * Get the number of parameters in neural network including all the
         * number of elements in matrix W and all the number of elements in
         * vector b
         *
         * @return sum of all elements in W and b
         */
        @Override
        public int getNumParameters() {
            int ret = 0;
            for (DoubleMatrix w : bpParam.getW()) {
                ret += w.length;
            }
            for (DoubleMatrix b : bpParam.getB()) {
                ret += b.length;
            }
            return ret;
        }

        /**
         * Flat matrix W and vector b into a one-dimesion array
         *
         * @param doubles ouput parameter array
         */
        @Override
        public void getParameters(double[] doubles) {
            int idx = 0;
//            for(int i = 0; i < bpParam.getW().length; i++){
//                for(int j = 0; j < bpParam.getW()[i].length; j++){
//                    doubles[idx++] = bpParam.getW()[i].get(j);
//                }
//            }
//            for(int i = 0; i <bpParam.getB().length; i++){
//                for(int j = 0; j < bpParam.getB()[i].length; j++){
//                    doubles[idx++] = bpParam.getB()[i].get(j);
//                }
//            }
            for (DoubleMatrix w : bpParam.getW()) {
                for (int row = 0; row < w.rows; row++) {
                    for (int column = 0; column < w.columns; column++) {
                        doubles[idx++] = w.get(row, column);
                    }
                }
            }
            for (DoubleMatrix b : bpParam.getB()) {
                for (int row = 0; row < b.rows; row++) {
                    for (int column = 0; column < b.columns; column++) {
                        doubles[idx++] = b.get(row, column);
                    }
                }
            }
        }

        /**
         * Get value of ith parameter
         *
         * @param arg
         * @return ith parameter
         */
        @Override
        public double getParameter(int arg) {
            int total = 0;
            //traverse all matrix W
            for (DoubleMatrix w : bpParam.getW()) {
                //check if arg in W[i]
                if (arg < (total + w.length)) {
                    int row = (arg - total) / w.columns;
                    int colum = (arg - total) % w.columns;
                    return w.get(row, colum);
                } else {
                    total += w.length;
                    continue;
                }
            }

            for (DoubleMatrix b : bpParam.getB()) {
                if (arg < (total + b.length)) {
                    int row = (arg - total) / b.columns;
                    int col = (arg - total) % b.columns;
                    return b.get(row, col);
                } else {
                    total += b.length;
                    continue;
                }
            }
            return 0;
        }

        /**
         * Set a flat array parameter into W matrices and b vectors
         *
         * @param doubles input flat array parameter
         */
        @Override
        public void setParameters(double[] doubles) {
            int idx = 0;
            for (DoubleMatrix w : bpParam.getW()) {
                for (int row = 0; row < w.rows; row++) {
                    for (int column = 0; column < w.columns; column++) {
                        w.put(row, column, doubles[idx++]);
                    }
                }
            }

            for (DoubleMatrix b : bpParam.getB()) {
                for (int row = 0; row < b.rows; row++) {
                    for (int column = 0; column < b.columns; column++) {
                        b.put(row, column, doubles[idx++]);
                    }
                }
            }
        }

        /**
         * To set parameter, need to find which W matrix or b vector hold the
         * ith position
         *
         * @param i position
         * @param d value
         */
        @Override
        public void setParameter(int i, double d) {
            int idx = 0;
            for (DoubleMatrix w : bpParam.getW()) {
                idx += w.length;
                if (i < idx) {
                    int row = (idx - w.length) / w.columns;
                    int column = (idx - w.length) % w.columns;
                    w.put(row, column, d);
                    return;
                }
            }

            for (DoubleMatrix b : bpParam.getB()) {
                idx += b.length;
                if (i < idx) {
                    int row = (idx - b.length) / b.columns;
                    int column = (idx - b.length) % b.columns;
                    b.put(row, column, d);
                    return;
                }
            }
        }

    }

}

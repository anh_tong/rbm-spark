/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.Backpropagation;

import com.anhth12.downpourSGD.train.SGDParam;
import java.util.Random;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Administrator
 */
public class BackpropagationParam extends SGDParam {

    private static final long serialVersionUID = 1L;
    private static Random rand = new Random(System.currentTimeMillis());

    private DoubleMatrix[] w;
    private DoubleMatrix[] b;

    protected int numberOfLayer;
    
    private BackpropagationParam(){}
    
    public BackpropagationParam(int in, int out, int[] hiddens){
        this(in, out, hiddens, null, null);
    }

    public BackpropagationParam(int in, int out, int[] hiddens, DoubleMatrix[] w, DoubleMatrix[] b) {
        int l = hiddens.length + 1;
        numberOfLayer = l + 1;
        this.w = new DoubleMatrix[l];
        this.b = new DoubleMatrix[l];

        for (int i = 0; i < l; i++) {
            int currentIn = -1;
            int currentOut = -1;
            if (i == 0) {
                currentIn = in;
            } else {
                currentIn = hiddens[i - 1];
            }
            if (i == (l - 1)) {
                currentOut = out;
            } else {
                currentOut = hiddens[i];
            }

            //w
            if (w != null && w[i] != null) {
                this.w[i] = w[i].dup();
            } else {
                this.w[i] = new DoubleMatrix(currentOut, currentIn);
                for (int j = 0; j < currentOut; j++) {
                    for (int k = 0; k < currentIn; k++) {
                        this.w[i].put(j, k, rand.nextGaussian()*0.01);
                    }
                }
            }
            
            if (b != null && b[i] !=null) {
                this.b[i] = b[i].dup();
            }else{
                this.b[i] = new DoubleMatrix(currentOut);
                for (int j = 0; j < currentOut; j++) {
                    this.b[i].put(j, 0, rand.nextGaussian()*0.01);
                }
            }

        }
    }

    public DoubleMatrix[] getW() {
        return w;
    }

    public DoubleMatrix[] getB() {
        return b;
    }

    public int getNumberOfLayer() {
        return numberOfLayer;
    }
    
    @Override
    protected SGDParam dup() {
        BackpropagationParam ret = new BackpropagationParam();
        ret.w = new DoubleMatrix[w.length];
        for (int i = 0; i < ret.w.length; i++) {
            ret.w[i] = w[i].dup();
        }
        ret.b = new DoubleMatrix[b.length];
        for (int i = 0; i < ret.b.length; i++) {
            ret.b[i] = b[i].dup();
        }
        ret.numberOfLayer = numberOfLayer;
        return ret;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.logisticregression;

import com.anhth12.downpourSGD.train.SGDParam;
import org.jblas.DoubleMatrix;

/**
 *
 * @author Administrator
 */
public class LogisticRegressionParam extends SGDParam {

    private static final long serialVersionUID = 1L;
    private DoubleMatrix w;
    private DoubleMatrix b;

    public LogisticRegressionParam(int numFeature, int numClass, double[][] w, double[] b) {
        if (w == null) {
            this.w = new DoubleMatrix(numClass, numFeature);
        } else {
            this.w = new DoubleMatrix(w);
        }

        if (b == null) {
            this.b = new DoubleMatrix(numClass);
        } else {
            this.b = new DoubleMatrix(b);
        }
    }

    public LogisticRegressionParam(int numFeature, int numClass) {
        this(numFeature, numClass, null, null);
    }

    private LogisticRegressionParam(){
    }

    public DoubleMatrix getW() {
        return w;
    }

    public DoubleMatrix getB() {
        return b;
    }

    @Override
    protected SGDParam dup() {
        LogisticRegressionParam ret = new LogisticRegressionParam();
        ret.w = w;
        ret.b = b;
        return ret;
    }
}

package com.anhth12.downpourSGD.logisticregression;

import cc.mallet.optimize.Optimizable;
import com.anhth12.downpourSGD.train.MyConjugateGradient;
import com.anhth12.downpourSGD.train.SGDBase;
import com.anhth12.downpourSGD.train.SGDParam;
import com.anhth12.downpourSGD.train.SGDTrainConfig;
import com.anhth12.downpourSGD.train.SampleVector;
import com.anhth12.downpourSGD.util.MathUtil;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import org.apache.log4j.Logger;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author anhth12
 */
public final class LogisticRegression extends SGDBase {

    private final static Logger logger = Logger.getLogger(LogisticRegression.class);
    private final static long serialVersionUID = 1L;

    private int numX;
    private int numY;

    private LogisticRegressionParam logisticRegressionParam;

    /**
     * Constructor without initial matrix W and bias vector b 
     * 
     */
    public LogisticRegression(int numFeature, int numClass) {
        this(numFeature, numClass, null, null);
    }

    /**
     * Full constructor of LogisticRegression 
     * 
     */
    public LogisticRegression(int numFeature, int numClass, double[][] w, double[] b) {
        numX = numFeature;
        numY = numClass;
        logisticRegressionParam = new LogisticRegressionParam(numFeature, numClass, w, b);
        super.param = logisticRegressionParam;
    }

    /**
     * Predict y from input x
     * @param x input vector
     * @return calculated ouput from trained model
     */
    public final DoubleMatrix predict(DoubleMatrix x) {
        DoubleMatrix y = x.mmul(logisticRegressionParam.getW().transpose())
                .addiRowVector(logisticRegressionParam.getB());
        //softmax(y)
        softmax(y);
        return y;
    }

    /**
     * Predict in form of double array
     * @param x input vector
     * @param y output vector
     */
    public final void predict(double[] x, double[] y) {
        for (int i = 0; i < numY; i++) {
            y[i] = 0;
            for (int j = 0; j < numX; j++) {
                y[i] += logisticRegressionParam.getW().get(i, j) * x[j];
            }
            y[i] += logisticRegressionParam.getB().get(i, 0);
        }
        //softmax y
        softmax(y);
    }

    /**
     * Softmax function or Sigmoid function 1/(1 + exp(-x))
     * @param y input
     */
    private void softmax(DoubleMatrix y) {
        DoubleMatrix max = y.rowMaxs();
        MatrixFunctions.expi(y.subiColumnVector(max));
        DoubleMatrix sum = y.rowSums();
        y.diviColumnVector(sum);
    }

    /**
     * Softmax function in form of double array
     * @param y input
     */
    private void softmax(double[] y) {
        double max = 0.0;
        double sum = 0.0;
        for (int i = 0; i < numY; i++) {
            if (max < y[i]) {
                max = y[i];
            }
        }

        for (int i = 0; i < numY; i++) {
            y[i] = Math.exp(y[i] - max);
            sum += y[i];
        }

        for (int i = 0; i < numY; i++) {
            y[i] /= sum;
        }
    }

    public DoubleMatrix getW() {
        return logisticRegressionParam.getW();
    }

    public DoubleMatrix getB() {
        return logisticRegressionParam.getB();
    }

    public int getNumX() {
        return numX;
    }

    public int getNumY() {
        return numY;
    }

    /**
     * Calculate and update Gradient based on Mini Batch strategy
     * TODO: provide link
     * This function will be called if SGDTraining config enable this function and configuration
     * necessary parameters
     * @param config config object
     * @param xSample training data x
     * @param ySample label data y
     * @param currentParam contains current training weight and bias
     */
    @Override
    protected void gradientUpdateMiniBatch(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam) {
        int numSamples = xSample.rows;
        DoubleMatrix currentW = ((LogisticRegressionParam) currentParam).getW();
        DoubleMatrix currentB = ((LogisticRegressionParam) currentParam).getB();

        DoubleMatrix currentPredictY = xSample.mmul(currentW.transpose()).addiRowVector(currentB);
        softmax(currentPredictY);
        DoubleMatrix deltaB = ySample.sub(currentPredictY);
        DoubleMatrix deltaW = deltaB.transpose().mmul(xSample);

        deltaB = deltaB.columnSums().divi(numSamples);
        deltaW.divi(numSamples);
        if (config.useRegulation) {
            if (config.lambda1 != 0) {
                deltaW.addi(MatrixFunctions.signum(currentW).mmuli(config.lambda1));
                deltaB.addi(MatrixFunctions.signum(currentB).mmuli(config.lambda1));
            }
            if (config.lambda2 != 0) {
                deltaW.addi(currentW.mmuli(config.lambda2));
                deltaB.add(currentB.mmuli(config.lambda2));
            }
        }
        currentW.addi(deltaW.mmuli(config.learningRate));
        currentB.addi(deltaB.mmul(config.learningRate));

    }
    /**
     * Calculate and update Gradient based on Conjugate Gradient
     * TODO: provide link
     * This function will be called if SGDTraining config enable this function and configuration
     * necessary parameters
     * @param config config object
     * @param xSample training data x
     * @param ySample label data y
     * @param currentParam contains current training weight and bias
     */
    @Override
    protected void gradientUpdateCG(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam) {
        DoubleMatrix currentW = ((LogisticRegressionParam) currentParam).getW();
        DoubleMatrix currentB = ((LogisticRegressionParam) currentParam).getB();

        LogisticRegressionOptimizer optimizer = new LogisticRegressionOptimizer(currentW, currentB, xSample, ySample, xSample.rows, config);

        MyConjugateGradient cg = new MyConjugateGradient(optimizer, config.cgInitStepSize);
        try {
            cg.optimize(config.cgMaxIterations);
        } catch (Throwable e) {
            logger.error("", e);
        }
    }

    /**
     * Merge param after each epoch of training
     * When using multithread training or training logisic regression on Spark, we need collect every seperated
     * weight matrix and bias vector
     * @param param SGD param including weight and bias for seperated thread or spliteed RDD
     * @param numModelReplica the number of threads or the number of spilled RDD across Spark cluster
     */
    @Override
    protected void mergeParam(SGDParam param, int numModelReplica) {
        LogisticRegressionParam newParam = (LogisticRegressionParam) param;
        logisticRegressionParam.getW().addi(newParam.getW().sub(logisticRegressionParam.getW()).divi(numModelReplica));
        logisticRegressionParam.getB().addi(newParam.getB().sub(logisticRegressionParam.getB()).divi(numModelReplica));
    }

    /**
     * Calculate loss value between predict value and test or train value 
     * @param samples list of vectors
     */
    @Override
    protected double lost(List<SampleVector> samples) {
        DoubleMatrix x_samples = MathUtil.convertX2Matrix(samples);
        DoubleMatrix y_samples = MathUtil.convertY2Matrix(samples);
        DoubleMatrix predict_y = predict(x_samples);
        return MatrixFunctions.powi(predict_y.sub(y_samples), 2).sum();
    }

    @Override
    public void read(DataInput in) throws IOException {
        numX = in.readInt();
        numY = in.readInt();
        for (int i = 0; i < numY; i++) {
            for (int j = 0; j < numX; j++) {
                logisticRegressionParam.getW().put(i, j, in.readDouble());
            }
        }
        for (int i = 0; i < numY; i++) {
            logisticRegressionParam.getB().put(i, 0, in.readDouble());
        }
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(numX);
        out.writeInt(numY);

        for (int i = 0; i < numY; i++) {
            for (int j = 0; j < numX; j++) {
                out.writeDouble(logisticRegressionParam.getW().get(i, i));
            }
        }
        for (int i = 0; i < numY; i++) {
            out.writeDouble(logisticRegressionParam.getB().get(i, 0));
        }
    }

    @Override
    public void print(Writer wr) throws IOException {
        String newLine = System.getProperty("line.separator");
        wr.write(String.valueOf(numX));
        wr.write(",");
        wr.write(String.valueOf(numY));
        wr.write(newLine);
        for (int i = 0; i < numY; i++) {
            for (int j = 0; j < numX; j++) {
                wr.write(String.valueOf(logisticRegressionParam.getW().get(i, i)));
                wr.write(",");
            }
            wr.write(newLine);
        }
        for (int i = 0; i < numY; i++) {
            wr.write(String.valueOf(logisticRegressionParam.getB().get(i, 0)));
            wr.write(",");
        }
        wr.write(newLine);
    }

    @Override
    protected boolean isSupervised() {
        return true;

    }

    private class LogisticRegressionOptimizer implements Optimizable.ByGradientValue {

        private DoubleMatrix myW;
        private DoubleMatrix myB;
        private DoubleMatrix myXSamples;
        private DoubleMatrix myYSmaples;
        private DoubleMatrix currentPredictY;
        private int numSamples;
        private SGDTrainConfig config;

        public LogisticRegressionOptimizer(DoubleMatrix myW, DoubleMatrix myB, DoubleMatrix myXSamples, DoubleMatrix myYSmaples, int numSamples, SGDTrainConfig config) {
            this.myW = myW;
            this.myB = myB;
            this.myXSamples = myXSamples;
            this.myYSmaples = myYSmaples;
            this.numSamples = numSamples;
            this.config = config;
        }

        @Override
        public void getValueGradient(double[] doubles) {
            DoubleMatrix deltaB = myYSmaples.sub(currentPredictY);
            DoubleMatrix deltaW = deltaB.transpose().mmul(myXSamples);

            deltaB = deltaB.columnSums().divi(numSamples);
            deltaW.divi(numSamples);

            if (config.useRegulation) {
                if (config.lambda1 != 0) {
                    deltaW.addi(MatrixFunctions.signum(myW).mmuli(config.lambda1));
                    deltaB.addi(MatrixFunctions.signum(myB).transpose()).mmuli(config.lambda1);
                }
                if (config.lambda2 != 0) {
                    deltaW.addi(myW.mmul(config.lambda2));
                    deltaB.addi(myB.transpose().mmul(config.lambda2));
                }
            }
            int idx = 0;
            for (int i = 0; i < numY; i++) {
                for (int j = 0; j < numX; j++) {
                    doubles[idx++] = deltaW.get(i, j);
                }
            }
            for (int i = 0; i < numY; i++) {
                doubles[idx++] = deltaB.get(0, i);
            }
        }

        @Override
        public double getValue() {
            currentPredictY = myXSamples.mmul(myW.transpose()).addiRowVector(myB);
            softmax(currentPredictY);
            double loss = MatrixFunctions.powi(currentPredictY.sub(myYSmaples), 2).sum() / numSamples;
            if (config.useRegulation) {
                if (config.lambda1 != 0) {
                    loss += config.lambda1
                            * (MatrixFunctions.abs(myW).sum() + MatrixFunctions.abs(myB).sum());
                }
                if (config.lambda2 != 0) {
                    loss += 0.5 * config.lambda2
                            * (MatrixFunctions.pow(myW, 2).sum() + MatrixFunctions.pow(myB, 2).sum());
                }
            }
            return -loss;
        }

        @Override
        public int getNumParameters() {
            return numX * numY + numY;
        }

        @Override
        public void getParameters(double[] doubles) {
            int idx = 0;
            for (int i = 0; i < numY; i++) {
                for (int j = 0; j < numX; j++) {
                    doubles[idx++] = logisticRegressionParam.getW().get(i, j);
                }
            }
            for (int i = 0; i < numY; i++) {
                doubles[idx++] = logisticRegressionParam.getB().get(i, 0);
            }
        }

        @Override
        public double getParameter(int i) {
            if (i < numX * numY) {
                int x = i / numX;
                int y = i % numX;
                return myW.get(x, y);
            }
            return myB.get(i - numX * numY, 0);
        }

        @Override
        public void setParameters(double[] doubles) {
            int idx = 0;
            for (int i = 0; i < numY; i++) {
                for (int j = 0; j < numX; j++) {
                    logisticRegressionParam.getW().put(i, j, doubles[idx++]);
                }
            }
            for (int i = 0; i < numY; i++) {
                logisticRegressionParam.getB().put(i, 0, doubles[idx++]);
            }
        }

        @Override
        public void setParameter(int i, double d) {
            if (i < numX * numY) {
                int x = i / numX;
                int y = i % numX;
                logisticRegressionParam.getW().put(x, y, d);
            } else {
                logisticRegressionParam.getB().put(i - numX * numY, 0, d);
            }
        }

    }

}

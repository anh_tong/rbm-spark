package com.anhth12.downpourSGD.tiedweightlayer;

import com.anhth12.downpourSGD.train.MyConjugateGradient;
import com.anhth12.downpourSGD.train.SGDParam;
import com.anhth12.downpourSGD.train.SGDTrainConfig;
import com.anhth12.downpourSGD.util.MathUtil;
import org.apache.log4j.Logger;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author anhth12
 */
public class AutoEncoder extends HiddenLayer {

    private static Logger logger = Logger.getLogger(AutoEncoder.class);

    public AutoEncoder(int numIn, int numOut) {
        super(numIn, numIn);
    }

    public AutoEncoder(int numIn, int numOut, double[][] w, double[] b) {
        super(numIn, numOut, w, b);
    }

    @Override
    public DoubleMatrix recontruct(DoubleMatrix x) {
        DoubleMatrix ret = x.mmul(hiddenLayerParam.getW().transpose()).addiRowVector(hiddenLayerParam.gethBias());
        MathUtil.sigmoid(ret);
        ret = ret.mmul(hiddenLayerParam.getW()).addiRowVector(hiddenLayerParam.getvBias());
        return ret;
    }

    @Override
    public void recontruct(double[] x, double[] reconstructed) {
        DoubleMatrix xMatrix = new DoubleMatrix(x).transpose();
        DoubleMatrix recontructedMatrix = recontruct(xMatrix);

        for (int i = 0; i < nVisible; i++) {
            reconstructed[i] = recontructedMatrix.get(i, 0);
        }

    }

    @Override
    protected void gradientUpdateMiniBatch(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam) {
        int numberOfSample = xSample.getRows();
        DoubleMatrix currentW = ((HiddenLayerParam) currentParam).getW();
        DoubleMatrix currentHBias = ((HiddenLayerParam) currentParam).gethBias();
        DoubleMatrix currentVBias = ((HiddenLayerParam) currentParam).getvBias();

        //reconstruct
        DoubleMatrix tilde_x = null;
        DoubleMatrix y = null;
        DoubleMatrix z = null;

        if (config.doCorruption) {
            double p = 1 - config.corruptionLevel;
            tilde_x = getCorruptedInput(xSample, p);
            y = tilde_x.mmul(currentW.transpose()).addiRowVector(currentHBias);
        } else {
            y = xSample.mmul(currentW.transpose()).addiRowVector(currentHBias);
        }
        MathUtil.sigmoid(y);
        z = y.mmul(currentW).addiRowVector(currentVBias);
        MathUtil.sigmoid(z);

        //gradient update
        DoubleMatrix L_vbias = xSample.sub(z);
        DoubleMatrix L_hbias = L_vbias.mmul(currentW.transpose()).muli(y).muli(y.neg().addi(1));
        DoubleMatrix delta_w = null;
        if (config.doCorruption) {
            delta_w = L_hbias.transpose().mmul(tilde_x).addi(y.transpose().mmul(L_vbias));
        } else {
            delta_w = L_hbias.transpose().mmul(xSample).addi(y.transpose().mmul(L_vbias));
        }
        if (config.useRegulation) {
            //only L2 for autoencoder
            if (0 != config.lambda2) {
                delta_w.subi(currentW.mul(config.lambda2));
            }
        }
        delta_w.divi(numberOfSample);
        DoubleMatrix delta_hbias = L_hbias.columnSums().divi(numberOfSample);
        DoubleMatrix delta_vbias = L_vbias.columnSums().divi(numberOfSample);
        
        currentW.addi(delta_w.muli(config.lambda2));
        currentHBias.addi(delta_hbias.transpose().muli(config.lambda2));
        currentVBias.addi(delta_vbias.transpose().muli(config.learningRate));
    }

    @Override
    protected void gradientUpdateCG(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam) {
        DoubleMatrix currentW = ((HiddenLayerParam) currentParam).getW();
        DoubleMatrix currentHBias = ((HiddenLayerParam) currentParam).gethBias();
        DoubleMatrix currentVBias = ((HiddenLayerParam) currentParam).getvBias();

        dAOptimizer optimizer = new dAOptimizer(config, nVisible, nHidden, currentW, currentHBias, currentVBias, xSample);

        MyConjugateGradient cg = new MyConjugateGradient(optimizer, config.cgInitStepSize);
        cg.setTolerance(config.cgTolerance);
        try {
            cg.optimize(config.cgMaxIterations);
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private DoubleMatrix getCorruptedInput(DoubleMatrix x, double p) {
        DoubleMatrix ret = new DoubleMatrix(x.getRows(), x.getColumns());
        for (int row = 0; row < x.getRows(); row++) {
            for (int column = 0; column < x.getColumns(); column++) {
                ret.put(row, column, MathUtil.binomial(1, p));
            }
        }
        return ret;
    }

    class dAOptimizer extends HiddenLayerOptimizer {

        private DoubleMatrix tilde_x;
        private DoubleMatrix y;
        private DoubleMatrix z;

        public dAOptimizer(SGDTrainConfig config, int nVisible, int nHidden,
                DoubleMatrix w, DoubleMatrix hBias, DoubleMatrix vBias,
                DoubleMatrix samples) {
            super(config, nVisible, nHidden, w, hBias, vBias, samples);
            if (this.config.doCorruption) {
                double p = 1 - this.config.corruptionLevel;
                tilde_x = getCorruptedInput(this.samples, p);
            }
        }

        @Override
        public void getValueGradient(double[] doubles) {
            DoubleMatrix L_vBias = samples.sub(z);
            DoubleMatrix L_hBias = L_vBias.mmul(w.transpose()).muli(y).muli(y.neg().addi(1));
            DoubleMatrix deltaW = null;
            if (config.doCorruption) {
                deltaW = L_hBias.transpose().mmul(tilde_x).addi(y.transpose().mmul(L_vBias));
            } else {
                deltaW = L_hBias.transpose().mmul(samples).addi(y.transpose().mmul(L_vBias));
            }
            if (config.useRegulation) {
                if (config.lambda2 != 0) {
                    deltaW.subi(w.mul(config.lambda2));
                }
            }
            deltaW.divi(numOfSamples);
            DoubleMatrix deltaHBias = L_hBias.columnSums().divi(numOfSamples);
            DoubleMatrix deltaVBias = L_vBias.columnSums().divi(numOfSamples);

            int idx = 0;
            for (int row = 0; row < nHidden; row++) {
                for (int column = 0; column < nVisible; column++) {
                    doubles[idx++] = deltaW.get(row, column);
                }
            }
            for (int i = 0; i < nHidden; i++) {
                doubles[idx++] = deltaHBias.get(0, i);
            }
            for (int i = 0; i < nVisible; i++) {
                doubles[idx++] = deltaVBias.get(0, i);
            }

        }

        @Override
        public double getValue() {
            if (config.doCorruption) {
                y = tilde_x.mmul(w.transpose()).addiRowVector(hBias);
            } else {
                y = samples.mmul(w.transpose()).addiRowVector(hBias);
            }

            MathUtil.sigmoid(y);
            z = y.mmul(w).addiRowVector(vBias);
            MathUtil.sigmoid(z);

            double loss = MatrixFunctions.powi(samples.sub(z), 2).sum() / numOfSamples;
            if (config.useRegulation) {
                if (config.lambda2 != 0) {
                    loss += 0.5 * config.lambda2 * MatrixFunctions.pow(w, 2).sum();
                }
            }
            return -loss;
        }

    }

}

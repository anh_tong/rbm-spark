/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.tiedweightlayer;

import com.anhth12.downpourSGD.train.MyConjugateGradient;
import com.anhth12.downpourSGD.train.SGDBase;
import com.anhth12.downpourSGD.train.SGDParam;
import com.anhth12.downpourSGD.train.SGDTrainConfig;
import com.anhth12.downpourSGD.train.SampleVector;
import com.anhth12.downpourSGD.util.MathUtil;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import org.apache.log4j.Logger;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author anhth12
 */
public abstract class HiddenLayer extends SGDBase {
    
    private static final Logger logger = Logger.getLogger(HiddenLayer.class);
    
    protected int nVisible;
    protected int nHidden;
    
    protected HiddenLayerParam hiddenLayerParam;
    
    public HiddenLayer(int nVisible, int nHidden) {
        this(nVisible, nHidden, null, null);
    }
    
    public HiddenLayer(int numIn, int numOut, double[][] w, double[] b) {
        nVisible = numIn;
        nHidden = numOut;
        hiddenLayerParam = new HiddenLayerParam(numIn, numOut, w, b);
        param = hiddenLayerParam;
    }
    
    public final DoubleMatrix sigmoid(DoubleMatrix input) {
        DoubleMatrix ret = input.mmul(hiddenLayerParam.getW().transpose()).addiRowVector(hiddenLayerParam.gethBias());
        MathUtil.sigmoid(ret);
        return ret;
    }
    
    public final void sigmoid(double[] visibleX, double[] hiddenX) {
        for (int i = 0; i < nHidden; i++) {
            hiddenX[i] = 0;
            for (int j = 0; j < nVisible; j++) {
                hiddenX[i] += hiddenLayerParam.getW().get(i, j) * visibleX[j];
            }
            hiddenX[i] += hiddenLayerParam.gethBias().get(i, 0);
            hiddenX[i] = MathUtil.sigmoid(hiddenX[i]);
        }
    }
    
//    @Override
//    protected void gradientUpdateMiniBatch(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    
//    @Override
//    protected void gradientUpdateCG(SGDTrainConfig config, DoubleMatrix xSample, DoubleMatrix ySample, SGDParam currentParam) {
//        HiddenLayerOptimizer op = null;
//        MyConjugateGradient cg = new MyConjugateGradient(op, config.cgInitStepSize);
//        cg.setTolerance(config.cgTolerance);
//        try {
//            cg.optimize(config.cgMaxIterations);
//        } catch (Exception e) {
//            logger.error("", e);
//        }
//    }
    
    
    @Override
    protected void mergeParam(SGDParam param, int numModelReplica) {
       HiddenLayerParam newParam = (HiddenLayerParam) param;
       //param = (newParam-Param)/numReplica
       hiddenLayerParam.getW().addi(newParam.getW().sub(hiddenLayerParam.getW())).divi(numModelReplica);
       hiddenLayerParam.gethBias().addi(newParam.gethBias().sub(hiddenLayerParam.gethBias())).divi(numModelReplica);
       hiddenLayerParam.getvBias().addi(newParam.getvBias().sub(hiddenLayerParam.getvBias())).divi(numModelReplica);
    }
    
    @Override
    protected double lost(List<SampleVector> samples) {
        DoubleMatrix x = MathUtil.convertX2Matrix(samples);
        DoubleMatrix recontruct = recontruct(x);
        return MatrixFunctions.powi(recontruct.sub(x), 2).sum();
    }
    
    @Override
    protected boolean isSupervised() {
        return false;
    }
    
    @Override
    public void read(DataInput in) throws IOException {
        nVisible = in.readInt();
        nHidden = in.readInt();
        for (int row = 0; row < nHidden; row++) {
            for (int column = 0; column < nVisible; column++) {
                hiddenLayerParam.getW().put(row, column, in.readDouble());
            }
        }
        for (int i = 0; i < nHidden; i++) {
            hiddenLayerParam.gethBias().put(i, 0, in.readDouble());
        }
        for (int i = 0; i < nVisible; i++) {
            hiddenLayerParam.getvBias().put(i, 0, in.readDouble());
        }
    }
    
    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(nVisible);
        out.writeInt(nHidden);
        for (int row = 0; row < nHidden; row++) {
            for (int column = 0; column < nVisible; column++) {
                out.writeDouble(hiddenLayerParam.getW().get(row, column));
            }
        }
        for (int i = 0; i < nHidden; i++) {
            out.writeDouble(hiddenLayerParam.gethBias().get(i, 0));
        }
        for (int i = 0; i < nVisible; i++) {
            out.writeDouble(hiddenLayerParam.getvBias().get(i, 0));
        }
    }
    
    @Override
    public void print(Writer wr) throws IOException {
        String newLine = System.getProperty("line.separator");
        wr.write(String.valueOf(nVisible));
        wr.write(",");
        wr.write(String.valueOf(nHidden));
        wr.write(newLine);
        for (int i = 0; i < nHidden; i++) {
            for (int j = 0; j < nVisible; j++) {
                wr.write(String.valueOf(hiddenLayerParam.getW().get(i, j)));
                wr.write(",");
            }
            wr.write(newLine);
        }
        for (int i = 0; i < nHidden; i++) {
            wr.write(String.valueOf(hiddenLayerParam.gethBias().get(i, 0)));
            wr.write(",");
        }
        wr.write(newLine);
        for (int i = 0; i < nVisible; i++) {
            wr.write(String.valueOf(hiddenLayerParam.getvBias().get(i, 0)));
            wr.write(",");
        }
        wr.write(newLine);
    }
    
    
    public abstract DoubleMatrix recontruct(DoubleMatrix x);
    
    public abstract void recontruct(double[] x, double[] reconstructed);
    
}

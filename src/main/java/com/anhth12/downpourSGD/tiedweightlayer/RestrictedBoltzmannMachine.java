package com.anhth12.downpourSGD.tiedweightlayer;

import com.anhth12.downpourSGD.train.MyConjugateGradient;
import com.anhth12.downpourSGD.train.SGDParam;
import com.anhth12.downpourSGD.train.SGDTrainConfig;
import com.anhth12.downpourSGD.util.MathUtil;
import org.apache.log4j.Logger;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author anhth12
 */
public class RestrictedBoltzmannMachine extends HiddenLayer {

    private static final Logger logger = Logger.getLogger(RestrictedBoltzmannMachine.class);

    public RestrictedBoltzmannMachine(int numIn, int numOut) {
        super(numIn, numOut);
    }

    public RestrictedBoltzmannMachine(int numIn, int numOut, double[][] w, double[] b) {
        super(numIn, numOut, w, b);
    }

    @Override
    public DoubleMatrix recontruct(DoubleMatrix x) {
        DoubleMatrix ret = x.mmul(hiddenLayerParam.getW().transpose()).addiRowVector(hiddenLayerParam.hBias);

        MathUtil.sigmoid(ret);
        for (int row = 0; row < ret.rows; row++) {
            for (int column = 0; column < ret.columns; column++) {
                ret.put(row, column,
                        MathUtil.binomial(1, ret.get(row, column)));
            }
        }

        ret = ret.mmul(hiddenLayerParam.getW()).addiRowVector(hiddenLayerParam.vBias);
        MathUtil.sigmoid(ret);
        return ret;

    }

    @Override
    public void recontruct(double[] x, double[] reconstructed) {
        DoubleMatrix x_m = new DoubleMatrix(x).transpose();
        DoubleMatrix ret = recontruct(x_m);
        for (int i = 0; i < nVisible; i++) {
            reconstructed[i] = ret.get(0, i);
        }
    }

    @Override
    protected void gradientUpdateMiniBatch(SGDTrainConfig config, DoubleMatrix xSample,
            DoubleMatrix ySample, SGDParam currentParam) {
        int numOfSamples = xSample.getRows();
        DoubleMatrix currentW = ((HiddenLayerParam) currentParam).getW();
        DoubleMatrix currentHBias= ((HiddenLayerParam) currentParam).gethBias();
        DoubleMatrix currentVBias = ((HiddenLayerParam) currentParam).getvBias();
        
        DoubleMatrix v1Sample = xSample;
        DoubleMatrix h1Probability = new DoubleMatrix(numOfSamples, nHidden);
        DoubleMatrix h1Sample = new DoubleMatrix(numOfSamples, nHidden);
        DoubleMatrix v2Probability = new DoubleMatrix(numOfSamples, nVisible);
        DoubleMatrix v2Sample = new DoubleMatrix(numOfSamples, nVisible);
        DoubleMatrix h2Probability = new DoubleMatrix(numOfSamples, nHidden);
        
        sampleHiddenGivenVisible(v1Sample, h1Probability, h1Sample, currentW, currentHBias);
        if (config.useHitonCD1) {
            sampleVisibleGivenHidden(h1Sample, v2Probability, null, currentW, currentVBias);
            sampleHiddenGivenVisible(v2Probability, h2Probability, null, currentW, currentHBias);
        }else{
            sampleVisibleGivenHidden(h1Sample, v2Probability, v2Sample, currentW, currentVBias);
            sampleHiddenGivenVisible(v2Sample, h2Probability, null, currentW, currentHBias);
        }
        
        DoubleMatrix deltaW = null;
        DoubleMatrix deltaHBias = null;
        DoubleMatrix deltaVBias = null;
        if (config.useHitonCD1) {
            deltaW = h1Probability.transpose().mmul(v1Sample).subi(h2Probability.transpose().mmul(v2Probability));
            deltaHBias = h1Probability.sub(h2Probability).columnSums().divi(numOfSamples);
            deltaVBias = v1Sample.sub(v2Probability).columnSums().divi(numOfSamples);
        }else{
            deltaW = h1Sample.transpose().mmul(v1Sample).subi(h2Probability.transpose().mmul(v2Sample));
            deltaHBias = h1Sample.sub(h2Probability).columnSums().divi(numOfSamples);
            deltaVBias = v1Sample.sub(v2Sample).columnSums().divi(numOfSamples);
        }
        
        if (config.useRegulation) {
            if (config.lambda2 != 0) {
                deltaW.subi(currentW.mul(config.lambda2));
            }
        }
        
        deltaW.divi(numOfSamples);
        currentW.addi(deltaW.muli(config.learningRate));
        currentHBias.addi(deltaHBias.muli(config.learningRate));
        currentVBias.addi(deltaVBias.muli(config.learningRate));
    }

    @Override
    protected void gradientUpdateCG(SGDTrainConfig config, DoubleMatrix xSample,
            DoubleMatrix ySample, SGDParam currentParam) {
        DoubleMatrix currentW = ((HiddenLayerParam) currentParam).getW();
        DoubleMatrix currentHBias = ((HiddenLayerParam) currentParam).gethBias();
        DoubleMatrix currentVBias = ((HiddenLayerParam) currentParam).getvBias();

        RestrictedBoltzmannMachineOptimizer optimizer = new RestrictedBoltzmannMachineOptimizer(
                config, nVisible, nHidden, currentW, currentHBias, currentVBias,
                xSample);

        MyConjugateGradient cg = new MyConjugateGradient(optimizer, config.cgInitStepSize);
        cg.setTolerance(config.cgTolerance);
        try {
            cg.optimize(config.cgMaxIterations);
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private void sampleHiddenGivenVisible(DoubleMatrix vSample, DoubleMatrix hProbability,
            DoubleMatrix hSample, DoubleMatrix currentW, DoubleMatrix currentHBias) {
        hProbability.copy(vSample.mmul(currentW.transpose()).addiRowVector(currentHBias));
        MathUtil.sigmoid(hProbability);
        if (hSample != null) {
            for (int row = 0; row < hProbability.rows; row++) {
                for (int column = 0; column < hProbability.columns; column++) {
                    hSample.put(row, column,
                            MathUtil.binomial(
                                    1, hProbability.get(row, column))
                    );
                }
            }
        }
    }

    private void sampleVisibleGivenHidden(DoubleMatrix hSample, DoubleMatrix vProbabiliry,
            DoubleMatrix vSample, DoubleMatrix currentW, DoubleMatrix currentVBias) {
        vProbabiliry.copy(hSample.mmul(currentW).addiRowVector(currentVBias));
        MathUtil.sigmoid(vProbabiliry);
        if (vSample != null) {
            for (int row = 0; row < vProbabiliry.rows; row++) {
                for (int column = 0; column < vProbabiliry.columns; column++) {
                    vSample.put(row, column,
                            MathUtil.binomial(
                                    1, vProbabiliry.get(row, column))
                    );
                }
            }
        }
    }

    public class RestrictedBoltzmannMachineOptimizer extends HiddenLayerOptimizer {

        private DoubleMatrix v1Sample;
        private DoubleMatrix h1Probability;
        private DoubleMatrix h1Sample;
        private DoubleMatrix v2Probability;
        private DoubleMatrix v2Sample;
        private DoubleMatrix h2Probability;

        public RestrictedBoltzmannMachineOptimizer(SGDTrainConfig config, int nVisible, int nHidden, DoubleMatrix w, DoubleMatrix hBias, DoubleMatrix vBias, DoubleMatrix samples) {
            super(config, nVisible, nHidden, w, hBias, vBias, samples);
            this.v1Sample = samples;
            this.h1Probability = new DoubleMatrix(numOfSamples, nHidden);
            this.h1Sample = new DoubleMatrix(numOfSamples, nHidden);
            this.v2Probability = new DoubleMatrix(numOfSamples, nVisible);
            this.v2Sample = new DoubleMatrix(numOfSamples, nVisible);
            this.h2Probability = new DoubleMatrix(numOfSamples, nHidden);
        }

        @Override
        public void getValueGradient(double[] doubles) {
            if (config.useHitonCD1) {
                sampleHiddenGivenVisible(v2Probability, h2Probability, null, w, hBias);
            } else {
                sampleHiddenGivenVisible(v2Sample, h2Probability, null, w, hBias);
            }

            DoubleMatrix deltaW = null;
            DoubleMatrix deltaHBias = null;
            DoubleMatrix deltaVBias = null;

            if (config.useHitonCD1) {
                deltaW = h1Probability.transpose().mmul(v1Sample).subi(h2Probability.transpose().mmul(v2Probability));
                deltaHBias = h1Probability.sub(h2Probability).columnSums().divi(numOfSamples);
                deltaVBias = v1Sample.sub(v2Probability).columnSums().divi(numOfSamples);
            } else {
                deltaW = h1Sample.transpose().mmul(v1Sample).subi(h2Probability.transpose().mmul(v2Sample));
                deltaHBias = h1Sample.sub(h2Probability).columnSums().divi(numOfSamples);
                deltaVBias = v1Sample.sub(v2Sample).columnSums().divi(numOfSamples);
            }

            if (config.useRegulation) {
                if (config.lambda2 != 0) {
                    deltaW.subi(w.mul(config.lambda2));
                }
            }
            deltaW.divi(numOfSamples);
            int idx = 0;
            for (int row = 0; row < nHidden; row++) {
                for (int column = 0; column < nVisible; column++) {
                    doubles[idx++] = deltaW.get(row, column);
                }
            }
            for (int i = 0; i < nHidden; i++) {
                doubles[idx++] = deltaHBias.get(0, i);
            }
            for (int i = 0; i < nVisible; i++) {
                doubles[idx++] = deltaVBias.get(0, i);
            }
        }

        @Override
        public double getValue() {
            sampleHiddenGivenVisible(v1Sample, h1Probability, h1Sample, w, hBias);
            if (config.useHitonCD1) {
                sampleVisibleGivenHidden(h1Sample, v2Probability, null, w, vBias);
            } else {
                sampleVisibleGivenHidden(h1Sample, v2Probability, v1Sample, w, vBias);
            }

            double loss = MatrixFunctions.powi(v1Sample.sub(v2Probability), 2).sum();
            if (config.useRegulation) {
                if (0 != config.lambda2) {
                    loss += 0.5 * config.lambda2 * MatrixFunctions.pow(w, 2).sum();
                }
            }
            return -loss;
        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.tiedweightlayer;

import cc.mallet.optimize.Optimizable;
import com.anhth12.downpourSGD.train.SGDTrainConfig;
import org.jblas.DoubleMatrix;

/**
 *
 * @author anhth12
 */
public abstract class HiddenLayerOptimizer implements Optimizable.ByGradientValue {

    protected SGDTrainConfig config;

    protected int nVisible;
    protected int nHidden;
    protected int numOfSamples;
    protected DoubleMatrix w;
    protected DoubleMatrix hBias;
    protected DoubleMatrix vBias;

    protected DoubleMatrix samples;

    public HiddenLayerOptimizer(SGDTrainConfig config, int nVisible, int nHidden,
            DoubleMatrix w, DoubleMatrix hBias, DoubleMatrix vBias, DoubleMatrix samples) {
        this.config = config;
        this.nVisible = nVisible;
        this.nHidden = nHidden;
        this.w = w;
        this.hBias = hBias;
        this.vBias = vBias;
        this.samples = samples;
        //
        this.numOfSamples = samples.getRows();

    }

    @Override
    public final int getNumParameters() {
        return nHidden * nVisible + nHidden + nVisible;
    }

    @Override
    public void getParameters(double[] doubles) {
        int idx = 0;
        for (int row = 0; row < nHidden; row++) {
            for (int column = 0; column < nVisible; column++) {
                doubles[idx++] = w.get(row, column);
            }
        }
        for (int i = 0; i < nHidden; i++) {
            doubles[idx++] = hBias.get(i, 0);
        }
        for (int i = 0; i < nVisible; i++) {
            doubles[idx++] = vBias.get(i, 0);
        }
    }

    @Override
    public final double getParameter(int i) {
        if (i < nVisible * nHidden) {
            int row = i / nVisible;
            int column = i % nVisible;
            return w.get(row, column);
        } else if (i < nVisible * nHidden + nHidden) {
            return hBias.get(i - nVisible * nHidden, 0);
        }
        return vBias.get(i - nVisible * nHidden - nHidden, 0);

    }

    @Override
    public void setParameters(double[] doubles) {
        int idx = 0;
        for (int row = 0; row < nHidden; row++) {
            for (int column = 0; column < nVisible; column++) {
                w.put(row, column, doubles[idx++]);
            }
        }
        for (int i = 0; i < nHidden; i++) {
            hBias.put(i, 0, doubles[idx++]);
        }
        for (int i = 0; i < nVisible; i++) {
            vBias.put(i, 0, doubles[idx++]);
        }
    }

    @Override
    public void setParameter(int i, double d) {
        if (i < nVisible * nHidden) {
            int row = i / nVisible;
            int column = i % nVisible;
            w.put(row, column, d);
        } else if (i < nVisible * nHidden + nHidden) {
            hBias.put(i - nVisible * nHidden, 0, d);
        } else {
            vBias.put(i - nVisible * nHidden - nHidden, 0, d);
        }
    }

}

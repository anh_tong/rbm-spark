/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.tiedweightlayer;

import com.anhth12.downpourSGD.train.SGDParam;
import com.anhth12.downpourSGD.util.MathUtil;
import org.jblas.DoubleMatrix;

/**
 *
 * @author anhth12
 */
public class HiddenLayerParam extends SGDParam{
    
    protected DoubleMatrix w;
    protected DoubleMatrix hBias;
    protected DoubleMatrix vBias;
    
    private HiddenLayerParam(){
        
    }
    
    public HiddenLayerParam(int in, int out, double[][] w, double[] b){
        int nVisible = in;
        int nHidden = out;
        if (w == null) {
            this.w = new DoubleMatrix(nHidden, nVisible);
            double a = 1.0 / nVisible;
            for (int row = 0; row < nHidden; row++) {
                for (int column = 0; column < nVisible; column++) {
                    this.w.put(row, column, MathUtil.uniform(-a, a));
                }
            }
        }else{
            this.w = new DoubleMatrix(w);
        }
        
        if (b == null) {
            this.hBias = new DoubleMatrix(nHidden);
        }else{
            this.hBias = new DoubleMatrix(b);
        }
        vBias = new DoubleMatrix(nVisible);
    }
    
    public HiddenLayerParam(int in, int out){
        this(in, out, null, null);
    }

    public DoubleMatrix getW() {
        return w;
    }

    public DoubleMatrix gethBias() {
        return hBias;
    }

    public DoubleMatrix getvBias() {
        return vBias;
    }

    @Override
    protected SGDParam dup() {
        HiddenLayerParam ret = new HiddenLayerParam();
        
        ret.w = w.dup();
        ret.hBias = hBias.dup();
        ret.vBias = vBias.dup();
        
        return ret;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.downpourSGD.util;

import com.anhth12.downpourSGD.train.SampleVector;
import java.util.List;
import java.util.Random;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author Administrator
 */
public class MathUtil {

    private static final Random rand = new Random(System.currentTimeMillis());

    /**
     * Convert sample X to matrix
     *
     * @param samples
     * @return
     */
    public static DoubleMatrix convertX2Matrix(List<SampleVector> samples) {
        int row = samples.size();
        int col = samples.get(0).getX().length;

        DoubleMatrix ret = new DoubleMatrix(row, col);
        row = 0;
        for (SampleVector sample : samples) {
            double[] x = sample.getX();
            for (col = 0; col < x.length; col++) {
                ret.put(row, col, x[col]);
            }
            row++;
        }
        return ret;
    }

    /**
     * Convert class Y to matrix
     *
     * @param samples
     * @return
     */
    public static DoubleMatrix convertY2Matrix(List<SampleVector> samples) {
        int row = samples.size();
        int col = samples.get(0).getY().length;

        DoubleMatrix ret = new DoubleMatrix(row, col);
        row = 0;
        for (SampleVector sample : samples) {
            double[] y = sample.getY();
            for (col = 0; col < y.length; col++) {
                ret.put(row, col, y[col]);
            }
            row++;
        }
        return ret;
    }

    public static void sigmoid(DoubleMatrix x) {
        MatrixFunctions.expi(x.negi()).addi(1.0).rdivi(1.0);
    }

    public static double sigmoid(double z) {
        return 1.0 / (1.0 + Math.exp(-z));
    }

    public static double uniform(double min, double max) {
        return rand.nextDouble() * (max - min) + min;
    }

    public static int binomial(int n, double p) {
        if ((p < 0) || (p > 1)) {
            return 0;
        }
        int c = 0;
        for (int i = 0; i < n; i++) {
            if (rand.nextDouble() < p) {
                c++;
            }
        }
        return c;
    }

}

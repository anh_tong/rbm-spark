/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anhth12.rbm;

import com.anhth12.param.Param;

import java.util.List;

/**
 *
 * @author lelightwin
 */
public class CFRBMwithIndex {

    public int M; // number of items
    public int H; // number of hidden features
    public int R; // number of ratings

    private double[][][] W; // matrix of weights
    private double[][] vbias; // visible bias
    private double[] hbias; // hidden bias

    private double[][][] deltaW;
    private double[][] deltaVBias;
    private double[] deltaHBias;

    private double[][][] errorW;
    private double[][] errorVBias;
    private double[] errorHBias;

    private Boolean[] posHStates;
    private Boolean[] negHStates;

    private int[] itemcount;

    private char[] negVSoftmax;
    private char[] posVSoftmax;
    private int T;

    //<editor-fold defaultstate="collapsed" desc="init function and constructor">
    public CFRBMwithIndex(int nVisble, int nHidden, int softmax) {
        this.M = nVisble;
        this.R = softmax;
        this.H = nHidden;
        this.W = new double[M][R][H];
        this.vbias = new double[M][R];
        this.hbias = new double[H];

        this.deltaW = new double[M][R][H];
        this.deltaVBias = new double[M][R];
        this.deltaHBias = new double[H];

        this.errorW = new double[M][R][H];
        this.errorVBias = new double[M][R];
        this.errorHBias = new double[H];

        this.posHStates = new Boolean[H];
        this.negHStates = new Boolean[H];

        this.itemcount = new int[M];
        this.negVSoftmax = new char[M];
        this.posVSoftmax = new char[M];
    }

    /**
     * set weights and biases (both visible and hidden) to zero
     */
    private void zeroWeights() {
        Function.zero(deltaW, M, R, H);
        Function.zero(deltaVBias, M, R);
        Function.zero(deltaHBias, H);
    }

    /**
     * set all intermediate data structure for calculation to zero
     */
    private void zeroCalculation() {
        Function.zero(errorW, M, R, H);
        Function.zero(errorHBias, H);
        Function.zero(errorVBias, M, R);
        Function.zero(itemcount, M);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="function for training">
    /**
     * calculate expectation for gradient ascent method
     *
     * @param trainInstance - vector of train instance
     * @param T - number of Contrastive Divergence
     */

    public void calculateGradientAscent(int[] trainInstance, int T) {
        int[] itemIndexes = new int[trainInstance.length];

        for (int i = 0; i < trainInstance.length; i++) {
            int m = trainInstance[i] / 10; // get index of item
            int r = trainInstance[i] % 10; // get rate for item
            posVSoftmax[m] = (char) r;
            itemIndexes[i] = m;
            itemcount[m]++; // increase count of item m, for normalize
        }
        //generate hidden states by visible provided by trainingInstance
        generateHgivenV(itemIndexes, posVSoftmax, posHStates);

        //<editor-fold defaultstate="collapsed" desc="loop Contrastive Divergence T times">
        generateVgivenH(posHStates, itemIndexes, negVSoftmax);
        generateHgivenV(itemIndexes, negVSoftmax, negHStates);

        for (int t = 1; t < T; t++) {
            generateVgivenH(negHStates, itemIndexes, negVSoftmax);
            generateHgivenV(itemIndexes, negVSoftmax, negHStates);
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="calculate positive and negative expectation">
        for (int m : itemIndexes) {
            errorVBias[m][posVSoftmax[m]] += 1.0; // calculate positive visible bias
            errorVBias[m][negVSoftmax[m]] -= 1.0; // calculate negative visible bias

            for (int h = 0; h < H; h++) {
                if (posHStates[h]) {
                    errorW[m][posVSoftmax[m]][h] += 1.0;
                }
                if (negHStates[h]) {
                    errorW[m][negVSoftmax[m]][h] -= 1.0;
                }
            }
        }

        for (int h = 0; h < H; h++) {
            if (posHStates[h]) {
                errorHBias[h] += 1.0; // calculate positive hidden bias
            }
            if (negHStates[h]) {
                errorHBias[h] -= 1.0; // calculate negative hidden bias
            }
        }
        //</editor-fold>
    }

    public Param train(List<int[]> trainData, int curIteration) {
        int k = 1;

        if (curIteration >= 50) {
            k = 3 + (curIteration - 50) / 25;
        }

        if (curIteration > 20) {
            DataInfo.momentum = DataInfo.finalMomentum;
        }

        int batchSize = trainData.size();

        for (int user = 0; user < trainData.size(); user++) {
            calculateGradientAscent(trainData.get(user), k);

//            if((user + 1) % 1000 == 0 || (user + 1) == trainData.size()){
//            }
        }
        update(batchSize);

        return new Param(deltaW, deltaVBias, deltaHBias, M, H, R);
    }

    /**
     * generate hidden states given visible states
     *
     * @param itemIndexes
     * @param softmax
     * @param hiddenStates
     */
    private void generateHgivenV(int[] itemIndexes, char[] softmax, Boolean[] hiddenStates) {
        double[] sumW = new double[H];
        Function.zero(sumW, H);

        for (int m : itemIndexes) {
            int r = softmax[m]; // get rate for item
            double[] hiddenW = W[m][r];
            for (int h = 0; h < H; h++) {
                sumW[h] += hiddenW[h]; // v[m][r]*W[m][r][h]
            }
        }

        for (int h = 0; h < H; h++) {
            double probH = Function.sigmoid(sumW[h] + hbias[h]); // probability of (h = 1)
            hiddenStates[h] = probH > Function.rand();
        }
    }

    /**
     * generate visible states given hidden states
     *
     * @param hiddenStates
     * @param itemIndexes
     * @param softmax
     */
    private void generateVgivenH(Boolean[] hiddenStates, int[] itemIndexes, char[] softmax) {
        double[] negViProbs = new double[R];
        Function.zero(negViProbs, R);
        for (int m : itemIndexes) {
            Function.zero(negViProbs, R);
            //<editor-fold defaultstate="collapsed" desc="calculate negative visible probability">
            double tsum = 0;

            for (int r = 0; r < R; r++) {
                double[] hiddenW = W[m][r];
                for (int h = 0; h < H; h++) {
                    if (hiddenStates[h]) {
                        negViProbs[r] += hiddenW[h];
                    }
                }
                negViProbs[r] = Function.sigmoid(negViProbs[r] + vbias[m][r]);
                tsum += negViProbs[r];
            }

            //Normalize probability
            if (tsum != 0) {
                for (int r = 0; r < R; r++) {
                    negViProbs[r] /= tsum;
                }
            }
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="sampling softmax by above probability">
            double randval = Function.rand();
            for (int r = 0; r < R; r++) {
                if ((randval -= negViProbs[r]) <= 0.0) {
                    softmax[m] = (char) r;
                    break;
                }
                softmax[m] = (char) (R - 1);
            }
            //</editor-fold>
        }
    }

    /**
     * update parameters with mini-batch
     *
     * @param batchNum - number of instance in batch
     */
    private void update(int batchNum) {
        for (int m = 0; m < M; m++) {
            if (itemcount[m] == 0) {
                continue;
            }

            // update weights
            for (int h = 0; h < H; h++) {
                for (int r = 0; r < R; r++) {
                    double error = errorW[m][r][h];
                    if (error != 0.0) {
                        error /= ((double) itemcount[m]);
                        double weightDecay = DataInfo.weightCost * W[m][r][h];
                        double momentumValue = DataInfo.momentum * deltaW[m][r][h];
                        deltaW[m][r][h] = momentumValue + DataInfo.epsilonW * (error - weightDecay);
//                        W[m][r][h] += deltaW[m][r][h];
                    }
                }
            }

            // update visible softmax biases
            for (int r = 0; r < R; r++) {
                double error = errorVBias[m][r];
                if (error != 0.0) {
                    error /= (double) itemcount[m];
                    double momentumValue = DataInfo.momentum * deltaVBias[m][r];
                    deltaVBias[m][r] = momentumValue + DataInfo.epsilonVB * error;
//                    vbias[m][r] += deltaVBias[m][r];
                }
            }
        }

        // update hidden bias
        for (int h = 0; h < H; h++) {
            double error = errorHBias[h];
            if (error != 0.0) {
                error /= (double) batchNum;
                double momentumValue = DataInfo.momentum * deltaHBias[h];
                deltaHBias[h] = momentumValue + DataInfo.epsilonHB * error;
//                hbias[h] += deltaHBias[h];
            }
        }
        zeroCalculation();
    }

    /**
     * set up parameters based on loop count
     *
     * @param loopcount
     */
    private void setArgument(int loopcount) {
        if (H >= 200) {
            if (loopcount > 6) {
                DataInfo.epsilonW *= 0.90;
                DataInfo.epsilonD *= 0.90;
                DataInfo.epsilonVB *= 0.90;
                DataInfo.epsilonHB *= 0.90;
            } else if (loopcount > 5) {  // With 200 hidden variables, you need to slow things down a little more
                DataInfo.epsilonW *= 0.50;         // This could probably use some more optimization
                DataInfo.epsilonD *= 0.50;
                DataInfo.epsilonVB *= 0.50;
                DataInfo.epsilonHB *= 0.50;
            } else if (loopcount > 2) {
                DataInfo.epsilonW *= 0.70;
                DataInfo.epsilonD *= 0.70;
                DataInfo.epsilonVB *= 0.70;
                DataInfo.epsilonHB *= 0.70;
            }
        } else {
            if (loopcount > 8) {
                DataInfo.epsilonW *= 0.92;
                DataInfo.epsilonD *= 0.92;
                DataInfo.epsilonVB *= 0.92;
                DataInfo.epsilonHB *= 0.92;
            } else if (loopcount > 6) {
                DataInfo.epsilonW *= 0.90;
                DataInfo.epsilonD *= 0.90;
                DataInfo.epsilonVB *= 0.90;
                DataInfo.epsilonHB *= 0.90;
            } else if (loopcount > 2) {
                DataInfo.epsilonW *= 0.78;
                DataInfo.epsilonD *= 0.78;
                DataInfo.epsilonVB *= 0.78;
                DataInfo.epsilonHB *= 0.78;
            }
        }
        if (loopcount >= 11) {
            T = 3 + 2 * (loopcount - 10) / 10;
        }
    }

    public void setParam(Param param){
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < R; j++) {
                this.vbias[i][j] = param.vbias()[i][j];
                for (int k = 0; k < H; k++) {
                    this.W[i][j][k] = param.weight()[i][j][k];
                }
            }
        }
        for (int k = 0; k < H; k++) {
            this.hbias[k] = param.hbias()[k];
        }
    }
}
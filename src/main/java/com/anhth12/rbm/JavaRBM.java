package com.anhth12.rbm;

import EDU.oswego.cs.dl.util.concurrent.FJTask;
import com.anhth12.param.Param;
import org.apache.spark.Logging;
import scala.Tuple2;
import scala.xml.PrettyPrinter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Tong Hoang Anh on 3/27/2015.
 */

public class JavaRBM implements Serializable{

    private double[][][] W; // matrix of weights
    private double[][] vbias; // visible bias
    private double[] hbias; // hidden bias

    private double[][][] deltaW;
    private double[][] deltaVBias;
    private double[] deltaHBias;

    private double[][][] posW;
    private double[][][] negW;

    private double[] posHBias;
    private double[] negHBias;
    private Boolean[] posHStates;
    private Boolean[] negHStates;

    private Boolean[] currentHStates; // store current hidden states

    private double[][] posVBias;
    private double[][] negVBias;

    private int[] itemcount;

    private double[] sumW;
    private double[][] negVProbs;
    private char[] negVSoftmax;
    private char[] posVSoftmax;

    public static int nVisbile;
    public static int nHidden;
    public static int softmax;

    public JavaRBM(int nVisbile, int softmax, int nHidden) {
        this.nVisbile = nVisbile;
        this.softmax = softmax;
        this.nHidden = nHidden;

        this.deltaW = new double[nVisbile][softmax][nHidden];
        this.deltaVBias = new double[nVisbile][softmax];
        this.deltaHBias = new double[nHidden];

        this.posW = new double[nVisbile][softmax][nHidden];
        this.negW = new double[nVisbile][softmax][nHidden];

        this.posHBias = new double[nHidden];
        this.negHBias = new double[nHidden];
        this.posHStates = new Boolean[nHidden];
        this.negHStates = new Boolean[nHidden];

        this.currentHStates = new Boolean[nHidden];

        this.posVBias = new double[nVisbile][softmax];
        this.negVBias = new double[nVisbile][softmax];
        this.itemcount = new int[nVisbile];
        this.sumW = new double[nHidden];
        this.negVProbs = new double[nVisbile][softmax];
        this.negVSoftmax = new char[nVisbile];
        this.posVSoftmax = new char[nVisbile];
    }

    public void setParam(Param param){
        this.W = param.weight();
        this.hbias = param.hbias();
        this.vbias = param.vbias();
    }

    private void generateHgivenV(int[] itemIndexes, char[] softmax, Boolean[] hiddenStates) {
        Function.zero(sumW, nHidden);

        for (int m : itemIndexes) {
            int r = softmax[m]; // get rate for item
            for (int h = 0; h < nHidden; h++) {
                sumW[h] += W[m][r][h]; // v[m][r]*W[m][r][h]
            }
        }

        for (int h = 0; h < nHidden; h++) {
            double probH = Function.sigmoid(sumW[h] + hbias[h]); // probability of (h = 1)
            hiddenStates[h] = probH > Function.rand();
        }
    }

    private void generateVgivenH(Boolean[] hiddenStates, int[] itemIndexes, char[] softmax) {
        Function.zero(negVProbs, nVisbile, this.softmax);
        for (int m : itemIndexes) {

            //<editor-fold defaultstate="collapsed" desc="calculate negative visible probability">
            for (int h = 0; h < nHidden; h++) {
                if (hiddenStates[h]) {
                    for (int r = 0; r < this.softmax; r++) {
                        negVProbs[m][r] += W[m][r][h];
                    }
                }
            }
            //<editor-fold defaultstate="collapsed" desc="Normalize probability">
            double tsum = 0;
            for (int r = 0; r < this.softmax; r++) {
                negVProbs[m][r] = Function.sigmoid(negVProbs[m][r] + vbias[m][r]);
                tsum += negVProbs[m][r];
            }

            if (tsum != 0) {
                for (int r = 0; r < this.softmax; r++) {
                    negVProbs[m][r] /= tsum;
                }
            }
            //</editor-fold>
            //</editor-fold>

            //<editor-fold defaultstate="collapsed" desc="sampling softmax by above probability">
            double randval = Function.rand();
            for (int r = 0; r < this.softmax; r++) {
                if ((randval -= negVProbs[m][r]) <= 0.0) {
                    softmax[m] = (char) r;
                    break;
                }
                softmax[m] = (char) (this.softmax - 1);
            }
            //</editor-fold>
        }
    }

    private void zeroWeights() {
        Function.zero(deltaW, nVisbile, softmax, nHidden);
        Function.zero(deltaVBias, nVisbile, softmax);
        Function.zero(deltaHBias, nHidden);
    }

    /**
     * set all intermediate data structure for calculation to zero
     */
    private void
    zeroCalculation() {
        Function.zero(posW, nVisbile, softmax, nHidden);
        Function.zero(negW, nVisbile, softmax, nHidden);
        Function.zero(posHBias, nHidden);
        Function.zero(negHBias, nHidden);
        Function.zero(posVBias, nVisbile, softmax);
        Function.zero(negVBias, nVisbile, softmax);
        Function.zero(itemcount, nVisbile);
    }

    public void calculateGradientAscent(int[] trainInstance, int T) {
        int[] itemIndexes = new int[trainInstance.length];

        Function.zero(negVProbs, nVisbile, softmax);
        for (int i = 0; i < trainInstance.length; i++) {
            int m = trainInstance[i] / 10; // get index of item
            int r = trainInstance[i] % 10; // get rate for item
            posVSoftmax[m] = (char) r;
            itemIndexes[i] = m;
            itemcount[m]++; // increase count of item m, for normalize
        }
        //<editor-fold defaultstate="collapsed" desc="generate hidden states by visible provided by trainingInstance">
        generateHgivenV(itemIndexes, posVSoftmax, posHStates);
        System.arraycopy(posHStates, 0, currentHStates, 0, nHidden);

        for (int t = 0; t < T; t++) {
            generateVgivenH(currentHStates, itemIndexes, negVSoftmax);
            generateHgivenV(itemIndexes, negVSoftmax, negHStates);
            System.arraycopy(negHStates, 0, currentHStates, 0, nHidden);
        }
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="loop Contrastive Divergence T times">
//        generateVgivenH(posHStates, itemIndexes, negVSoftmax);
//        generateHgivenV(itemIndexes, negVSoftmax, negHStates);
//        for (int t = 1; t < T; t++) {
//            generateVgivenH(negHStates, itemIndexes, negVSoftmax);
//            generateHgivenV(itemIndexes, negVSoftmax, negHStates);
//        }

        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="calculate positive and negative expectation">
        for (int m : itemIndexes) {
            posVBias[m][posVSoftmax[m]] += 1.0; // calculate positive visible bias
            negVBias[m][negVSoftmax[m]] += 1.0; // calculate negative visible bias

            for (int h = 0; h < nHidden; h++) {
                if (posHStates[h]) {
                    posW[m][posVSoftmax[m]][h] += 1.0;
                }
                if (negHStates[h]) {
                    negW[m][negVSoftmax[m]][h] += 1.0;
                }
            }
        }

        for (int h = 0; h < nHidden; h++) {
            if (negHStates[h]) {
                negHBias[h] += 1.0; // calculate negative hidden bias
            }
            if (posHStates[h]) {
                posHBias[h] += 1.0; // calculate positive hidden bias
            }
        }
        //</editor-fold>
    }

    public Param train(List<int[]> input, int curIteration){

        int K = 1;
        if (curIteration >= 50) {
            K = 3 + (curIteration - 50) / 25;
        }

        if (curIteration > 20) {
            DataInfo.momentum = DataInfo.finalMomentum;
        }
        int batchSize = input.size();
        zeroCalculation();

        for (int[] user : input) {
            calculateGradientAscent(user, K);
        }

        for (int m = 0; m < nVisbile; m++) {
            if (itemcount[m] == 0) {
                continue;
            }

            // update weights
            for (int h = 0; h < nHidden; h++) {
                for (int r = 0; r < softmax; r++) {
                    double Wdata = posW[m][r][h];
                    double Wmodel = negW[m][r][h];
                    if (Wdata != 0.0 || Wmodel != 0.0) {
                        Wdata /= ((double) itemcount[m]);
                        Wmodel /= ((double) itemcount[m]);

                        double error = Wdata - Wmodel;
                        System.out.println("W error: " + error);
                        double weightDecay = DataInfo.weightCost * W[m][r][h];
                        double momentumValue = DataInfo.momentum * deltaW[m][r][h];
                        deltaW[m][r][h] = momentumValue + DataInfo.epsilonW * (error - weightDecay);
                    }
                }
            }

            // update visible softmax biases
            for (int r = 0; r < softmax; r++) {
                if (posVBias[m][r] != 0.0 || negVBias[m][r] != 0.0) {
                    posVBias[m][r] /= (double) itemcount[m];
                    negVBias[m][r] /= (double) itemcount[m];

                    double error = posVBias[m][r] - negVBias[m][r];
                    System.out.println("vBias error: " + error);
                    double momentumValue = DataInfo.momentum * deltaVBias[m][r];
                    deltaVBias[m][r] = momentumValue + DataInfo.epsilonVB * error;
//                    vbias[m][r] += deltaVBias[m][r];
                }
            }
        }

        // update hidden bias
        for (int h = 0; h < nHidden; h++) {
            if (posHBias[h] != 0.0 || negHBias[h] != 0.0) {
                posHBias[h] /= (double) batchSize;
                negHBias[h] /= (double) batchSize;

                double error = posHBias[h] - negHBias[h];
                System.out.println("hBias error: " + error);
                double momentumValue = DataInfo.momentum * deltaHBias[h];
                deltaHBias[h] = momentumValue + DataInfo.epsilonHB * error;
//                hbias[h] += deltaHBias[h];
            }
        }

        zeroCalculation();
        Param gradient = new Param(deltaW, deltaVBias, deltaHBias, nHidden, nVisbile, softmax);
//        if(curIteration %3 == 1){
//            setArgument(curIteration);
//        }
        return gradient;
    }

    private void setArgument(int loopcount) {
        if (nHidden == 200) {
            if (loopcount > 30) {
                DataInfo.epsilonW *= 0.90;
                DataInfo.epsilonD *= 0.90;
                DataInfo.epsilonVB *= 0.90;
                DataInfo.epsilonHB *= 0.90;
            } else if (loopcount > 25) {  // With 200 hidden variables, you need to slow things down a little more
                DataInfo.epsilonW *= 0.50;         // This could probably use some more optimization
                DataInfo.epsilonD *= 0.50;
                DataInfo.epsilonVB *= 0.50;
                DataInfo.epsilonHB *= 0.50;
            } else if (loopcount > 10) {
                DataInfo.epsilonW *= 0.70;
                DataInfo.epsilonD *= 0.70;
                DataInfo.epsilonVB *= 0.70;
                DataInfo.epsilonHB *= 0.70;
            }
        } else {
            if (loopcount > 40) {
                DataInfo.epsilonW *= 0.92;
                DataInfo.epsilonD *= 0.92;
                DataInfo.epsilonVB *= 0.92;
                DataInfo.epsilonHB *= 0.92;
            } else if (loopcount > 30) {
                DataInfo.epsilonW *= 0.90;
                DataInfo.epsilonD *= 0.90;
                DataInfo.epsilonVB *= 0.90;
                DataInfo.epsilonHB *= 0.90;
            } else if (loopcount > 10) {
                DataInfo.epsilonW *= 0.78;
                DataInfo.epsilonD *= 0.78;
                DataInfo.epsilonVB *= 0.78;
                DataInfo.epsilonHB *= 0.78;
            }
        }
    }

    public static Tuple2<Double, Integer> evaluate(int[] trainSet, int[] testSet, Param param, int nVisbile, int nHidden, int softmax){
        double[][] negVProbs1 = new double[nVisbile][softmax];
        double[] posHProbs = new double[nHidden];

        int testNumber = testSet.length;

        double rmse = 0;

        double[] sumW = new double[nHidden];

        for(Integer i: trainSet){
            int m = i / 10;
            int r = i % 10;

            for(int h = 0; h < nHidden; h++){
                sumW[h] +=param.weight()[m][r][h];
            }
        }

        for (int h = 0; h < nHidden; h++) {
            posHProbs[h] = Function.sigmoid(sumW[h] + param.hbias()[h]);
        }

        for(Integer i: testSet){
            int m = i / 10;
            for (int h = 0; h < nHidden; h++) {
                for (int r = 0; r < softmax; r++) {
                    negVProbs1[m][r] += posHProbs[h] * param.weight()[m][r][h];
                }
            }

            double tsum = 0;
            for (int r = 0; r < softmax; r++) {
                negVProbs1[m][r] = Function.sigmoid(negVProbs1[m][r] + param.vbias()[m][r]);
                tsum += negVProbs1[m][r];
            }

            if (tsum != 0) {
                for (int r = 0; r < softmax; r++) {
                    negVProbs1[m][r] /= tsum;
                }
            }
        }
        for (int i = 0; i < testNumber; i++) {
            int m = testSet[i] / 10;
            int goldenRate = testSet[i] % 10;

            double predict = 0;
            for (int r = 0; r < softmax; r++) {
                predict += (r + 1) * negVProbs1[m][r];
            }
            double errors = goldenRate + 1 - predict;
            rmse += errors * errors;
        }

        return new Tuple2<>(rmse, testNumber);

    }

    private static JavaRBM instance = null;

    public static JavaRBM getInstance(){
        if (instance == null){
            instance = new JavaRBM(nVisbile,softmax, nHidden);
        }
        return instance;
    }
}


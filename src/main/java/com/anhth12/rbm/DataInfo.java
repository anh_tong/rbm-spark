package com.anhth12.rbm;

import java.io.Serializable;

/**
 * Created by Tong Hoang Anh on 3/27/2015.
 */
public class DataInfo implements Serializable{

    public static double epsilonW = 0.01;

    public static double epsilonD = 0.01;
    /**
     * Learning rate for weights
     */
    public static double epsilonVB = 0.01;
    /**
     * Learning rate for biases of visible units
     */
    public static double epsilonHB = 0.01;
    /**
     * Learning rate for biases of hidden units
     */
    public static double weightCost = 0.001; // weight decay
    public static double momentum = 0.8;
    public static double finalMomentum = 0.8;
}

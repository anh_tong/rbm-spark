package com.anhth12.rbm;

import java.io.Serializable;

/**
 * Created by Tong Hoang Anh on 4/9/2015.
 */
public class Parameter implements Serializable {
    private double[][][] deltaW;
    private double[][] deltaVBias;
    private double[] deltaHBias;

    public Parameter(double[][][] deltaW, double[][] deltaVBias, double[] deltaHBias) {
        this.deltaW = deltaW;
        this.deltaVBias = deltaVBias;
        this.deltaHBias = deltaHBias;
    }

    public double[][][] getDeltaW() {
        return deltaW;
    }

    public double[][] getDeltaVBias() {
        return deltaVBias;
    }

    public double[] getDeltaHBias() {
        return deltaHBias;
    }

}

package com.anhth12.rbm;

import java.util.Random;

/**
 * Created by Tong Hoang Anh on 3/27/2015.
 */
public class Function {

    public static Random rng = new Random();

    /**
     *
     * @param min
     * @param max
     * @return a value within duration (min, max)
     */
    public static double uniform(double min, double max) {
        return rng.nextDouble() * (max - min) + min;
    }

    public static void zero(double[] a, int l1) {

        for (int i = 0; i < l1; i++) {
            a[i] = 0;
        }
    }

    public static void zero(int[] a, int l1) {

        for (int i = 0; i < l1; i++) {
            a[i] = 0;
        }
    }

    public static void zero(int[][] a, int l1, int l2) {

        for (int i = 0; i < l1; i++) {
            for (int j = 0; j < l2; j++) {
                a[i][j] = 0;
            }
        }
    }

    public static void zero(double[][] a, int l1, int l2) {

        for (int i = 0; i < l1; i++) {
            for (int j = 0; j < l2; j++) {
                a[i][j] = 0.0;
            }
        }
    }

    public static void zero(double[][][] a, int l1, int l2, int l3) {

        for (int i = 0; i < l1; i++) {
            for (int j = 0; j < l2; j++) {
                for (int k = 0; k < l3; k++) {
                    a[i][j][k] = 0.0;
                }
            }
        }
    }

    public static double rand() {
        return rng.nextDouble();
    }

    public static double sigmoid(double x) {
        return 1.0 / (1.0 + Math.exp(-x));
    }
}

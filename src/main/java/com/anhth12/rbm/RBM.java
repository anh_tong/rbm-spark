package com.anhth12.rbm;

import com.anhth12.param.Param;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

/**
 * Created by Tong Hoang Anh on 4/8/2015.
 */
public class RBM implements Serializable {

    private static Random randn = new Random();

    public static int nVisbile;
    public static int nHidden;
    public static int softmax;

    private double[][][] W; // matrix of weights
    private double[][] vbias; // visible bias
    private double[] hbias; // hidden bias

    private static double[][][] CDpos;// = new double[nVisbile][softmax][nHidden];
    private static double[][][] CDneg;// = new double[nVisbile][softmax][nHidden];
    private static double[][][] CDinc;// = new double[nVisbile][softmax][nHidden];

    private static double[] poshidact;// = new double[nHidden];
    private static double[] neghidact;// = new double[nHidden];
    private static char[] poshidstates;// = new char[nHidden];
    private static char[] neghidstates;// = new char[nHidden];
    private static double[] hidbiasinc;// = new double[nHidden];

    private static char[] curposhidstates;// = new char[nHidden];

    private static double[][] posvisact;// = new double[nVisbile][nHidden];
    private static double[][] negvisact;// = new double[nVisbile][nHidden];
    private static double[][] visbiasinc;// = new double[nVisbile][nHidden];
    private static double[][] negvisprobs;// = new double[nVisbile][nHidden];

    private static char[] negvissoftmax;// = new char[nVisbile];
    private static int[] moviecount;// = new int[nVisbile];

    public RBM(){
        CDpos = new double[nVisbile][softmax][nHidden];
        CDneg = new double[nVisbile][softmax][nHidden];
        CDinc = new double[nVisbile][softmax][nHidden];

        poshidact = new double[nHidden];
        neghidact = new double[nHidden];
        poshidstates = new char[nHidden];
        neghidstates = new char[nHidden];
        hidbiasinc = new double[nHidden];

        curposhidstates = new char[nHidden];

        posvisact = new double[nVisbile][nHidden];
        negvisact = new double[nVisbile][nHidden];
        visbiasinc = new double[nVisbile][nHidden];
        negvisprobs = new double[nVisbile][nHidden];

        negvissoftmax = new char[nVisbile];
        moviecount = new int[nVisbile];
    }

    public void setParam(Param param){
        this.W = param.weight();
        this.hbias = param.hbias();
        this.vbias = param.vbias();
    }


    private static void Zero() {
        Function.zero(CDpos, nVisbile, softmax, nHidden);
        Function.zero(CDneg, nVisbile, softmax, nHidden);
        Function.zero(poshidact, nHidden);
        Function.zero(neghidact, nHidden);
        Function.zero(posvisact, nVisbile, softmax);
        Function.zero(negvisact, nVisbile, softmax);
        Function.zero(moviecount, nVisbile);
    }

    public Param train(List<int[]> trainingSet, int currentIter) {

        int tSteps = 1;

//        if (currentIter >= 10) {
//            tSteps = 3 + (currentIter - 10) / 5;
//        }
//        currentIter++;
//
//        if (currentIter > 5) {
//            DataInfo.momentum = DataInfo.finalMomentum;
//        }

        int batchSize = trainingSet.size();
        Zero();
        for (int user = 0; user < batchSize; user++) {

            int num = trainingSet.get(user).length;
            double[] sumW = new double[nVisbile];
            Function.zero(sumW, nHidden);

            Function.zero(negvisprobs, nVisbile, softmax);

            for (int i = 0; i < num; i++) {
                int m = trainingSet.get(user)[i] / 10;
                int r = trainingSet.get(user)[i] % 10;
                moviecount[m]++;

                posvisact[m][r] += 1.0;

                for (int h = 0; h < nHidden; h++) {
                    sumW[h] += W[m][r][h];
                }
            }

            for (int h = 0; h < nHidden; h++) {
                double probs = 1.0 / (1.0 + Math.exp(-sumW[h] - hbias[h]));
                if (probs > randn.nextDouble()) {
                    poshidstates[h] = 1;
                    poshidact[h] += 1.0;
                } else {
                    poshidstates[h] = 0;
                }
            }

            for (int h = 0; h < nHidden; h++) {
                curposhidstates[h] = poshidstates[h];
            }

            /**
             * Make T steps of Contrastive Divergence
             */
            int stepT = 0;
            do {
                boolean finalTStep = (stepT + 1 >= tSteps);

                for (int i = 0; i < num; i++) {
                    int m = trainingSet.get(user)[i] / 10;

                    for (int h = 0; h < nHidden; h++) {
                        if (curposhidstates[h] == 1) {
                            for (int r = 0; r < softmax; r++) {
                                negvisprobs[m][r] += W[m][r][h];
                            }
                        }
                    }

                    for (int r = 0; r < softmax; r++) {
                        negvisprobs[m][r] = 1. / (1 + Math.exp(-negvisprobs[m][r] - vbias[m][r]));
                    }

                    /**
                     * Normalize probabilities
                     */
                    double tsum = 0;
                    for (int r = 0; r < softmax; r++) {
                        tsum += negvisprobs[m][r];
                    }

                    if (tsum != 0) {
                        for (int r = 0; r < softmax; r++) {
                            negvisprobs[m][r] /= tsum;
                        }
                    }

                    double randval = Function.rand();
                    for (int r = 0; r < this.softmax; r++) {
                        if ((randval -= negvisprobs[m][r]) <= 0.0) {
                            negvissoftmax[m] = (char) r;
                            break;
                        }
                        negvissoftmax[m] = (char) (this.softmax - 1);
                    }

                    if (finalTStep) {
                        negvisact[m][negvissoftmax[m]] += 1.0;
                    }
                }

                Function.zero(sumW, nHidden);
                for (int i = 0; i < num; i++) {
                    int m = trainingSet.get(user)[i] / 10;

                    for (int h = 0; h < nHidden; h++) {
                        sumW[h] += W[m][negvissoftmax[m]][h];
                    }
                }

                for (int h = 0; h < nHidden; h++) {
                    double probs = 1.0 / (1.0 + Math.exp(-sumW[h] - hbias[h]));

                    if (probs > randn.nextDouble()) {
                        neghidstates[h] = 1;
                        if (finalTStep) {
                            neghidact[h] += 1.0;
                        }
                    } else {
                        neghidstates[h] = 0;
                    }
                }

                if (!finalTStep) {
                    for (int h = 0; h < nHidden; h++) {
                        curposhidstates[h] = neghidstates[h];
                    }
                    Function.zero(negvisprobs, nVisbile, softmax);
                }

            } while (++stepT < tSteps);

            for (int i = 0; i < num; i++) {
                int m = trainingSet.get(user)[i] / 10;
                int r = trainingSet.get(user)[i] % 10;

                for (int h = 0; h < nHidden; h++) {
                    if (poshidstates[h] == 1) {
                        CDpos[m][r][h] += 1.0;
                    }
                    CDneg[m][negvissoftmax[m]][h] += (double) neghidstates[h];
                }
            }

        }
        return update(batchSize);
    }


    private Param update(int batchSize) {

        Param param = null;

        for (int m = 0; m < nVisbile; m++) {

            if (moviecount[m] == 0) {
                continue;
            }
            for (int h = 0; h < nHidden; h++) {

                for (int r = 0; r < softmax; r++) {
                    double CDp = CDpos[m][r][h];
                    double CDn = CDneg[m][r][h];
                    if (CDp != 0.0 || CDn != 0.0) {
                        CDp /= ((double) moviecount[m]);
                        CDn /= ((double) moviecount[m]);
                        CDinc[m][r][h] = DataInfo.momentum * CDinc[m][r][h] + DataInfo.epsilonW * ((CDp - CDn) - DataInfo.weightCost * W[m][r][h]);
                    }
                }
            }
            for (int r = 0; r < softmax; r++) {
                if (posvisact[m][r] != 0.0 || negvisact[m][r] != 0.0) {
                    posvisact[m][r] /= ((double) moviecount[m]);
                    negvisact[m][r] /= ((double) moviecount[m]);
                    visbiasinc[m][r] = DataInfo.momentum * visbiasinc[m][r] + DataInfo.epsilonVB * ((posvisact[m][r] - negvisact[m][r]));
                }
            }
        }
        for (int h = 0; h < nHidden; h++) {
            if (poshidact[h] != 0.0 || neghidact[h] != 0.0) {
                poshidact[h] /= ((double) (batchSize));
                neghidact[h] /= ((double) (batchSize));
                hidbiasinc[h] = DataInfo.momentum * hidbiasinc[h] + DataInfo.epsilonHB * ((poshidact[h] - neghidact[h]));
            }
        }

        Zero();
        return new Param(CDinc, visbiasinc, hidbiasinc,nVisbile, nHidden, softmax);
    }

    private static RBM instance = null;

    public static RBM getInstance(){
        if (instance == null){
            instance = new RBM();
        }
        return instance;
    }


}
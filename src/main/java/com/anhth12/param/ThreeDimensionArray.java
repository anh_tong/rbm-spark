package com.anhth12.param;

import akka.remote.transport.ThrottledAssociation;

import java.io.Serializable;

/**
 * Created by Tong Hoang Anh on 3/24/2015.
 */
public class ThreeDimensionArray implements Serializable{

    private int d1;
    private int d2;
    private int d3;

    private double[][][] array;

    public ThreeDimensionArray(int d1, int d2, int d3){
        this.d1 = d1;
        this.d2 = d2;
        this.d3 = d3;
        this.array = new double[d1][d2][d3];
    }

    public double get(int d1, int d2, int d3){
        return this.array[d1][d2][d3];
    }

    public void set(double value, int d1, int d2, int d3){
        this.array[d1][d2][d3] = value;
    }

    public void add(double value, int d1, int d2, int d3){
        this.array[d1][d2][d3] = value;
    }

    public void zeroItself(){
        for (int i = 0; i < d1; i++) {
            for (int j = 0; j < d2; j++) {
                for (int k = 0; k < d3; k++) {
                    this.array[i][j][k] = 0;
                }
            }
        }
    }

    public ThreeDimensionArray addi(ThreeDimensionArray other){
        for (int i = 0; i < d1; i++) {
            for (int j = 0; j < d2; j++) {
                for (int k = 0; k < d3; k++) {
                    this.array[i][j][k] += other.array[i][j][k];
                }
            }
        }
        return this;
    }

    public double[][][] array(){
        return this.array;
    }
}

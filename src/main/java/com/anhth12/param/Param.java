package com.anhth12.param;

import org.apache.spark.serializer.JavaDeserializationStream;

import java.io.*;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Created by Tong Hoang Anh on 3/24/2015.
 */
public class Param implements Serializable{

    private static transient Random random = new Random();

    private int nVisible;
    private int nHidden;
    private int softmax;

    private double[][][] weight;

    private double[][] vbias;

    private double[] hbias;

    private Param(){

    }

    public double[][][] weight(){
        return weight;
    }

    public double[][] vbias(){
        return vbias;
    }
    public double[] hbias(){
        return hbias;
    }


    public Param(int nVisible, int nHidden, int softmax){
        this.nVisible = nVisible;
        this.nHidden = nHidden;
        this.softmax = softmax;
        init();
    }

    public Param(ThreeDimensionArray weight, TwoDimensionArray vbias, OneDimensionArray hbias){
        this.weight =weight.array();
        this.vbias = vbias.array();
        this.hbias = hbias.array();
    }

    public Param(double[][][] weight, double[][] vbias, double[] hbias, int nVisible, int nHidden, int softmax){
        this.weight = weight;
        this.vbias = vbias;
        this.hbias = hbias;
        this.nVisible = nVisible;
        this.nHidden = nHidden;
        this.softmax = softmax;
    }



    private void init(){
        this.weight = new double[nVisible][softmax][nHidden];
        this.vbias = new double[nVisible][softmax];
        this.hbias = new double[nHidden];
    }

    public void initWeight(){
        for(int visible = 0; visible < nVisible; visible ++) {
            for (int rate = 0; rate < softmax; rate++) {
                for (int hidden = 0; hidden < nHidden; hidden++) {
                    this.weight[visible][rate][hidden] = 0.02 * random.nextDouble() - 0.01;
                }
            }
        }
    }

    public void initVbias(Map<Integer, Integer> movierate){
        Set<Map.Entry<Integer, Integer>> entrySet = movierate.entrySet();
        int[][] mr = new int[nVisible][softmax];
        for(Map.Entry<Integer, Integer> entry:  movierate.entrySet()){
            mr[entry.getKey()/10][entry.getKey()%10] = entry.getValue();
        }
        for(int i = 0; i < nVisible; i ++) {
            int mtot = 0;
            for (int k = 0; k < softmax; k++) {
                mtot += mr[i][k];
            }
            for (int k = 0; k < softmax; k++) {
                vbias[i][k] = Math.log(((double) mr[i][k]) / ((double) mtot));
            }
        }
    }


    public double getWeight(int visible, int softmax, int hidden){
        return this.weight[visible][softmax][hidden];
    }

    public double getVbias(int visible, int softmax){
        return this.vbias[visible][softmax];
    }

    public double getHbias(int hidden){
        return this.hbias[hidden];
    }

    public void setWeight(double value, int visible, int softmax, int hidden){
        this.weight[visible][softmax][hidden] = value;
    }

    public void setVbias(double value, int visible, int softmax){
        this.vbias[visible][softmax] = value;
    }

    public void setHbias(double value, int hidden){
        this.hbias[hidden] = value;
    }

    public void addWeight(double value, int visible, int softmax, int hidden){
        this.weight[visible][softmax][hidden] += value;
    }

    public void addVbias(double value, int visible, int softmax){
        this.vbias[visible][softmax] += value;
    }

    public void addHbias(double value, int hidden){
        this.hbias[hidden] += value;
    }

    public Param duplicate(){
        Param newParam = new Param();
        newParam.weight = this.weight.clone();
        newParam.hbias = this.hbias.clone();
        newParam.vbias = this.vbias.clone();
        return newParam;
    }

    public static Param duplicateFrom(double[][][] weight, double[][] vbias, double[] hbias){
        Param p = new Param();
        p.weight = weight;
        p.vbias= vbias;
        p.hbias = hbias;
        return p;
    }

    public Param scale(double scale){
        for(int visible = 0; visible < nVisible; visible ++){
            for(int rate = 0; rate < softmax; rate ++){
                this.vbias[visible][rate] = this.vbias[visible][rate] * scale;
                for(int hidden = 0; hidden < nHidden; hidden ++){
                    this.weight[visible][rate][hidden] = this.weight[visible][rate][hidden]*scale;
                }
            }
        }
        for(int hidden = 0; hidden < nHidden; hidden ++){
            this.hbias[hidden] = this.hbias[hidden] *scale;
        }
        return this;
    }


    public Param addi(Param newParam){
        for(int visible = 0; visible < nVisible; visible ++){
            for(int rate = 0; rate < softmax; rate ++){
                this.vbias[visible][rate] += newParam.vbias[visible][rate];
                for(int hidden = 0; hidden < nHidden; hidden ++){
                    this.weight[visible][rate][hidden] += newParam.weight[visible][rate][hidden];
                }
            }
        }
        for(int hidden = 0; hidden < nHidden; hidden ++){
            this.hbias[hidden] += newParam.hbias[hidden];
        }
        return this;
    }


    public Param subi(Param newParam){
        for(int visible = 0; visible < nVisible; visible ++){
            for(int rate = 0; rate < softmax; rate ++){
                this.vbias[visible][rate] -= newParam.vbias[visible][rate];
                for(int hidden = 0; hidden < nHidden; hidden ++){
                    this.weight[visible][rate][hidden] -= newParam.weight[visible][rate][hidden];
                }
            }
        }
        for(int hidden = 0; hidden < nHidden; hidden ++){
            this.hbias[hidden] -= newParam.hbias[hidden];
        }
        return this;
    }

    private void saveModel(ObjectOutputStream outputStream) throws IOException{
        outputStream.writeObject(this);
    }
    public int getnVisible() {
        return nVisible;
    }

    public int getSoftmax() {
        return softmax;
    }

    public int getnHidden() {
        return nHidden;
    }

    private Param readModel(ObjectInputStream inputStream) throws IOException, ClassNotFoundException{
        return (Param) inputStream.readObject();
    }

    public void saveModel(String fileName) throws Exception{
        ObjectOutputStream oos = null;
        try{
            FileOutputStream fout = new FileOutputStream("matrix.ser");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(this);
        }finally {
            if(oos != null) {
                oos.close();
            }
        }


    }


}

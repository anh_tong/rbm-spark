package com.anhth12.param;

import java.io.Serializable;

/**
 * Created by Tong Hoang Anh on 3/24/2015.
 */
public class OneDimensionArray implements Serializable{

    private int d;

    private double[] array;

    public OneDimensionArray(int d) {
        this.d = d;
        this.array = new double[d];
    }

    public double get(int d){
        return this.array[d];
    }

    public void set(double value, int d){
        this.array[d] = value;
    }

    public void add(double value, int d){
        this.array[d] += value;
    }

    public void zeroItself(){
        for (int i = 0; i < this.d; i++) {
            this.array[i] = 0;
        }
    }

    public OneDimensionArray addi(OneDimensionArray other){
        for (int i = 0; i < this.d; i++) {
            this.array[i] += other.array[i];
        }
        return this;
    }

    public double[] array(){
        return this.array;
    }
}

package com.anhth12.param;

import java.io.Serializable;

/**
 * Created by Tong Hoang Anh on 3/24/2015.
 */
public class TwoDimensionArray implements Serializable{
    private int d1;
    private int d2;

    private double[][] array;

    public TwoDimensionArray(int d1, int d2) {
        this.d1 = d1;
        this.d2 = d2;
        this.array = new double[d1][d2];
    }

    public double get(int d1, int d2){
        return this.array[d1][d2];
    }

    public void set(double value, int d1, int d2){
        this.array[d1][d2] = value;
    }

    public void add(double value, int d1, int d2){
        this.array[d1][d2] += value;
    }

    public void zeroItself(){
        for (int i = 0; i < d1; i++) {
            for (int j = 0; j < d2; j++) {
                this.array[i][j] = 0.0;
            }
        }
    }

    public TwoDimensionArray addi(TwoDimensionArray other){
        for (int i = 0; i < d1; i++) {
            for (int j = 0; j < d2; j++) {
                this.array[i][j] += other.array[i][j];
            }
        }
        return this;
    }

    public double[][] array(){
        return this.array;
    }
}

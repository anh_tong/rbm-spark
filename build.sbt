import sbt.Keys._
import AssemblyKeys._

assemblySettings

//name := "Word2VecExample"


  name := "word2vecexample"

  version := "1.0"

  scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.10" % "1.1.0" % "provided",
  "org.apache.spark" % "spark-mllib_2.10" % "1.1.0" % "provided",
  "cc.mallet" % "mallet" % "2.0.7-RC2",
  "com.typesafe.akka" % "akka-cluster_2.10" % "2.2.5"
//  "com.typesafe.akka" % "akka-cluster_2.10" % "2.2.5" // % "provided"
)

////local
//libraryDependencies ++= Seq(
//  "org.apache.spark" % "spark-core_2.10" % "1.1.0",
//  "org.apache.spark" % "spark-mllib_2.10" % "1.1.0",
//  "cc.mallet" % "mallet" % "2.0.7-RC2",
//  "com.typesafe.akka" % "akka-cluster_2.10" % "2.2.5",
//  "com.typesafe.akka" % "akka-actor_2.10" % "2.2.5"
//  //  "com.typesafe.akka" % "akka-cluster_2.10" % "2.2.5" // % "provided"
//)
//
//libraryDependencies += "org.apache.spark" % "spark-core_2.10" % "1.1.0" % "provided"
////% "provided"
//
//libraryDependencies += "org.apache.spark" % "spark-mllib_2.10" % "1.1.0" % "provided"
////% "provided"
//
//libraryDependencies += "cc.mallet" % "mallet" % "2.0.7-RC2"
//
////libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.4.0"
//
//
//libraryDependencies += "com.typesafe.akka" % "akka-cluster_2.10" % "2.2.5" % "provided"

val deployTask1 = TaskKey[Unit]("deploy", "Copies assembly jar file to server")


deployTask1 <<= assembly map { (asm) =>
  val account = "root@10.220.75.133"
  val local = asm.getPath
  val remote = account + ":/root/word2vec/" + asm.getName
  println(s"Copying $local -> $account:$remote")
  Seq("scp", local, remote)
}







